'use strict';

const Util = require('../commons-util');
const Const = require('../commons-const');
const { CreateHttpError } = require('../commons-errors');
const http = require('http');
const https = require('https');
const doRequest = require('request');
const path = require('path');
const _ = require("underscore");
const { promisify } = require('util');

class HttpClient {

    constructor(options) {

        if (!options.host) {
            Util.throwErrorIfItExists(new Error('The url can not be empty.'));
        }

        this.host = options.host || '';
        this.basePath = options.basePath || '';
        this.user = options.user;
        this.password = options.password || '';
        this.token = options.token || '';
        this.poolSize = options.poolSize || Const.DEFAULT_POOLSIZE;
        this.authentication = options.authentication || '';
        this.timeout = options.timeout || Const.DEFAULT_TIMEOUT_MS;
        this.protocol = this.host.substr(0, this.host.indexOf(':'));

    }

    generateRequestOptions(options) {

        let requestOptions = {
            method: options.method || Const.HTTP_METHOD.GET,
            url: this.host + path.join('/', this.basePath, '/', options.path || ''),
            qs: options.query || {},
            timeout: this.timeout,
            gzip: true,
            headers: this.generateRequestHeaders(options)
        };

        //For HealthCheck Routes
        if (options.monitor) {
            Util.info(`HealthCheck Routes - Host=(${this.host}) - Path=${options.path || ''}`);
            requestOptions.url = this.host + path.join('/', options.path || '');
            Util.info(`HealthCheck Routes - URL Final=(${requestOptions.url})`);
        }

        //Validate path windows
        requestOptions.url = requestOptions.url.replace(/\\/g, "/");

        _.extend(requestOptions, { agent: this.getAgent() });

        if (requestOptions.method == Const.HTTP_METHOD.POST &&
            requestOptions.headers[Const.HEADERS.CONTENT_TYPE] == Const.FORM_CONTENT_TYPE_VALUE) {

            if (!_.isEmpty(options.params)) {
                _.extend(requestOptions, { form: options.params })
            } else {
                _.extend(requestOptions, { form: options.body })
            }

        } else {
            if(!_.isEmpty(options.body)){
                _.extend(requestOptions, { body: JSON.stringify(options.body) })
            };
        }

        return requestOptions;

    };

    getAgent() {

        let agent = null;

        switch (this.protocol) {
            case 'https':
                agent = new https.Agent({ keepAlive: true });
                break;
            default:
                agent = new http.Agent({ keepAlive: true });
                break;
        };

        agent.maxSockets = this.poolSize || Const.DEFAULT_POOLSIZE;

        return agent;

    };

    generateRequestHeaders(options) {

        let headers = {};

        if (options.headers) {
            headers = _.extend(headers, options.headers);
        }

        if (this.authentication &&
            this.authentication.toLowerCase() === 'basic' &&
            this.user && this.password) {
            headers.Authorization = 'Basic ' + new Buffer(this.user + ':' + this.password).toString('base64');
        }

        if (this.authentication &&
            this.authentication.toLowerCase() === 'bearer' &&
            this.token) {
            headers.Authorization = 'Bearer ' + this.token;
        }

        /*if (options.method == Const.HTTP_METHOD.POST &&
            options.headers[Const.HEADERS.CONTENT_TYPE] == 'application/x-www-form-urlencoded;charset=UTF-8') {
            headers[Const.HEADERS.CONTENT_LENGTH] = querystring.stringify(options.params).length;
        } else {
            headers[Const.HEADERS.CONTENT_LENGTH] = querystring.stringify(options.body).length;
        }*/

        //headers.Connection = 'keep-alive';

        headers.Connection = 'close';

        return headers;

    };

    //TODO ADICIONAR O STEPFLOW PORQUE ELE TEM O CONTEXT
    execute(options, callbackStep) {

        let requestOptions = this.generateRequestOptions(options);

        Util.info(`Request (${this.protocol.toUpperCase()}) for Client Name=${options.name || ''}`, {
            uuid: options.uuid || '',
            flowStepUuid: options.flowStepUuid || '',
            contextUuid: options.contextUuid || '',
            timeout: requestOptions.timeout,
            method: requestOptions.method,
            url: requestOptions.url,
            body: Util.stringifyInfo(requestOptions.body || {}),
            query: Util.stringifyInfo(requestOptions.qs || {}),
            headers: Util.stringifyInfo(requestOptions.headers || {})
        });

        doRequest(requestOptions, (error, response) => {

            //TODO: TRATAR ESSES ERROS
            //error.code[EHOSTUNREACH,ECONNREFUSED,ECONNRESET,ESOCKETTIMEDOUT]

            if (error) Util.error(`Error Request (${this.protocol.toUpperCase()}) for Client Name=${options.name || ''}`, {
                uuid: options.uuid || '',
                flowStepUuid: options.flowStepUuid || '',
                contextUuid: options.contextUuid || '',
                timeout: requestOptions.timeout,
                method: requestOptions.method,
                url: requestOptions.url,
                body: Util.stringifyInfo(requestOptions.body || {}),
                query: Util.stringifyInfo(requestOptions.qs || {}),
                headers: Util.stringifyInfo(requestOptions.headers || {}),
                error: Util.stringifyInfo(error)
            });

            if (error || response.statusCode >= Const.HTTP_STATUS.ERROR_CLIENT_RANGE_START) {
                callbackStep(new CreateHttpError(error, response), null);
            } else {

                Util.info(`Response (${this.protocol.toUpperCase()}) for Client Name=${options.name || ''}`, {
                    uuid: options.uuid || '',
                    flowStepUuid: options.flowStepUuid || '',
                    contextUuid: options.contextUuid || '',
                    timeout: requestOptions.timeout,
                    method: requestOptions.method,
                    url: requestOptions.url,
                    body: Util.stringifyInfo(requestOptions.body || {}),
                    query: Util.stringifyInfo(requestOptions.qs || {}),
                    headers: Util.stringifyInfo(requestOptions.headers || {}),
                    result: Util.stringifyInfo(response.body || {}),
                });

                callbackStep(null, response);
            };

        });

    };
}

HttpClient.prototype.executePromise = promisify(HttpClient.prototype.execute);

module.exports = HttpClient;