'use strict';

const Util = require('./commons-util');
const async = require('async');
const Step = require('step');
const errors = require('./commons-errors');
const Const = require('./commons-const');
const _ = require('underscore');
const Env = require('./environment').application;

class OrchestrationFlow {

    constructor(redisClient) {
        this.redisClient = redisClient;
    }

    execute(routeContext, mainCallback) {

        let self = this;

        routeContext.service.init();

        async.mapSeries(routeContext.service.flow,
            (flowStep, callbackMapSeries) => {

                //STEP 1 - VALIDATE CONDITION AND VALIDATION TO EXECUTE FLOW
                if (self.checkCondition(flowStep, routeContext) &&
                    self.checkValidation(flowStep, routeContext)) {
                    self.executeFlowStep(flowStep, routeContext, callbackMapSeries);
                } else {
                    self.executeFlowFailed(flowStep, routeContext, callbackMapSeries);
                };

            },
            () => {
                routeContext.service.end();
                mainCallback();
            });

    };

    executeFlowFailed(flowStep, routeContext, callbackMapSeries) {

        let self = this;

        Util.info(`Skip operation flow(${flowStep.operation.type.toUpperCase()}) name=${flowStep.operation.name}`,
            {
                flowStepUuid: flowStep.uuid,
                contextUuid: routeContext.uuid
            });

        //Inicializa timers
        flowStep.init();
        routeContext.service.setCurrentFlow(flowStep);

        flowStep.result = flowStep.condition.whenFail.result;
        flowStep.statusCode = flowStep.condition.whenFail.statusCode;
        flowStep.error = flowStep.condition.whenFail.error;

        Step(
            //STEP 4 - UPDATE MAIN CONTEXT
            function () {
                self.updateRouteContext(flowStep, routeContext, this)
            },
            //STEP 5 - SKIP ERROR?
            function () {
                flowStep.end();
                callbackMapSeries(!flowStep.skipErrors && flowStep.error ? true : null);
            }
        );

    }

    executeFlowStep(flowStep, routeContext, callbackMapSeries) {

        let self = this;

        if (!flowStep.operation) {
            callback(new errors.ApiGatewayError('An operation must be provided to execute the flow.'));
        } else {

            //Inicializa timers
            flowStep.init();
            routeContext.service.setCurrentFlow(flowStep);

            Step(
                //STEP 1 - LOAD CACHE
                function () {
                    self.getResponseFlowFromCache(flowStep, routeContext, this);
                },
                //STEP 2 - VALIDATE CACHE / EXECUTE FLOW
                function (responseFlowCache) {
                    self.executeFlowOperation(responseFlowCache, flowStep, routeContext, this);
                },
                //STEP 4 - UPDATE MAIN CONTEXT
                function () {
                    self.updateRouteContext(flowStep, routeContext, this)
                },
                //STEP 5 - SKIP ERROR?
                function () {
                    flowStep.end();
                    callbackMapSeries(!flowStep.skipErrors && flowStep.error ? true : null);
                }
            );
        };
    };

    executeFlowOperation(responseFlowCache, flowStep, routeContext, callbackStep) {

        if (!responseFlowCache) {
            flowStep.operation.execute(flowStep, routeContext, () => {
                this.validateCache(flowStep, routeContext, callbackStep);
            });
        } else {
            flowStep.statusCode = responseFlowCache.statusCode;
            flowStep.setResult(Util.tryParseJSON(responseFlowCache.result));
            callbackStep();
        };

    };

    validateCache(flowStep, routeContext, callbackStep) {

        let self = this;
        //TODO VERIFICAR PQ TA NULL O redisClient
        if (this.redisClient.enableRedis &&
            flowStep.cacheEnable && !flowStep.error) {
            if (flowStep.getResult()) {
                this.redisClient.put(flowStep.cacheName || routeContext.service.name, self.generateCacheKey(flowStep, routeContext),
                    {
                        result: flowStep.getResult(),
                        statusCode: flowStep.statusCode
                    },
                    flowStep.cacheTtl);
            };
        };

        callbackStep();

    };

    getCompileContext(context) {
        return _.extend(context, {
            env: Env
        });
    };

    checkCondition(flowStep, routeContext) {

        let { condition } = flowStep;

        try {
            return condition.compileExpression === true || _.template(condition.compileExpression)(this.getCompileContext(routeContext)) === "true";
        } catch (error) {
            Util.error(`Error to compile condition=${condition}, error=${error}`);
            return false;
        }

    };

    checkValidation(flowStep, routeContext) {

        let { validation } = flowStep;

        try {
            if (validation !== true && _.template(validation)(this.getCompileContext(routeContext)) !== "true") {
                throw new errors.SecurityError("Unauthorized");
            } else {
                return true;
            }
        } catch (error) {
            Util.error(`Error to compile validation=${validation}, error=${error}`);
            return false;
        }

    };

    updateRouteContext(flowStep, routeContext, callbackStep) {

        //FOR RESPONSE TO CLIENT USE THIS RESULT
        if (flowStep.hasResult() ||
            (flowStep.resultVariable !== Const.OPERATION_RESULT_IS_ORCHESTRATION_RESULT)) {

            //Propagate Result Flow to Body for the Next Flow
            if (flowStep.propagateResult) {
                routeContext.setBody(flowStep.getResult());
            }

            //ADD ALL RESULTS TO SERVICE FOR HELP FLOWS
            routeContext.service.addResult(flowStep.getResult());
            routeContext.service.addHeaders(flowStep.responseHeaders);

            if (flowStep.resultVariable === Const.OPERATION_RESULT_IS_ORCHESTRATION_RESULT) {
                routeContext.setResult(flowStep.getResult());
            } else {
                routeContext.setResult(Util.setValue(routeContext.getResult(), flowStep.resultVariable, flowStep.getResult()));
            };

            routeContext.statusCode = flowStep.statusCode;
            routeContext.error = flowStep.error;

        };

        callbackStep();

    };

    compileCacheKey(cacheKey, routeContext) {

        let compiled = _.template(cacheKey);

        try {
            return compiled({
                security: routeContext.security,
                result: routeContext.getResult(),
                body: routeContext.body,
                query: routeContext.query,
                params: routeContext.params,
                headers: routeContext.headers,
                service: routeContext.service,
            });
        } catch (error) {
            Util.error(`Error to compile cacheKey=${cacheKey}, error=${error}`);
            return cacheKey;
        }

    };

    generateCacheKey(flowStep, routeContext) {

        let self = this;

        if (flowStep.cacheKey) {
            return self.compileCacheKey(flowStep.cacheKey, routeContext);
        } else {
            return self.getRequestDNA(routeContext);
        };

    };

    getRequestDNA(routeContext) {

        return Util.generateKey({
            url: {
                protocol: routeContext.request.isSecure() ? "https:" : "http:",
                hostname: routeContext.request.headers.host,
                port: routeContext.request.connection.localPort,
                path: routeContext.request.getPath()
            },
            socket: {
                remoteAddress: routeContext.request.socket.remoteAddress,
                encrypted: routeContext.request.isSecure()
            },
            httpVersion: routeContext.request.httpVersion,
            method: routeContext.request.method,
            body: routeContext.request.body || {},
            query: routeContext.query || {},
            params: routeContext.params || {},
            headers: routeContext.headers || {}
        });

    };

    getResponseFlowFromCache(flowStep, routeContext, callbackStep) {

        let self = this;

        if (this.redisClient.enableRedis &&
            flowStep.cacheEnable) {

            this.redisClient.get(flowStep.cacheName || routeContext.service.name,
                self.generateCacheKey(flowStep, routeContext),
                (error, data) => {
                    if (!data) {
                        callbackStep(null);
                    } else {
                        callbackStep(data);
                    }
                });

        } else {
            callbackStep(null);
        };

    };
};

module.exports = OrchestrationFlow;