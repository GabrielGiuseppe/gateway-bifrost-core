'use strict';

const _ = require('underscore');
const Const = require('../commons-const');
const Util = require('../commons-util');
const { Errors } = require('../commons-errors');
const BaseContext = require('../models/base-context');

class AbstractOperation extends BaseContext {

    constructor(uuid, context) {
        super(uuid, context);
    };

    execute(flowStepToUpdate, context, callbackFromOrchestrationFlow) {
        this.init();
        this.doExecute({ context: context, flowStep: flowStepToUpdate },
            (error, result) => {
                this.end();
                this.loggingResponse();
                this.handleResponse(error, result, flowStepToUpdate, callbackFromOrchestrationFlow);
            });
    };

    executeFunction(inputData, context, callbackStepOperation) {
        this.init();
        this.inputData = _.clone(inputData); //OBRIGAR A RETORNAR O OBJETO ALTERDO
        this.doExecuteFunction(inputData, context,
            (error, result) => {
                this.end();
                this.loggingResponse();
                callbackStepOperation(error, result);
            });
    };

    handleResponse(error, result, flowStepToUpdate, callbackFromOrchestrationFlow) {

        if (error) {
            let objectError = Errors.handleError(Util.parseJSON(error || {}));
            flowStepToUpdate.statusCode = objectError.responseCode || objectError.codigoResposta;
            flowStepToUpdate.error = true;
            flowStepToUpdate.setResult(Util.tryParseJSON(objectError));
            callbackFromOrchestrationFlow(objectError, null);
        } else {
            flowStepToUpdate.statusCode = Const.HTTP_STATUS.OK;
            flowStepToUpdate.error = false
            flowStepToUpdate.setResult(Util.tryParseJSON(result));
            callbackFromOrchestrationFlow(null, Util.tryParseJSON(result));
        };

    };

    loggingResponse() {
        Util.info(`Response operation function flow(${this.type.toUpperCase()}) name=${this.name}`, _.extend(this.getDefaultResponse(),
            { result: Util.stringifyInfo(this.getResult()) }));
    };

    getDefaultResponse() {
        return {
            flowStepUuid: this.uuid,
            contextUuid: this.context.uuid,
            functionName: this.name,
            duration: this.timers.duration
        }
    };

};

module.exports = AbstractOperation;