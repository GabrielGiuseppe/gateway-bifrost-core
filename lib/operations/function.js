'use strict';

const FunctionFactory = require('../factory/function-factory');
const AbstractOperation = require('./abstract-operation');
const Const = require('../commons-const');
const { CreateFunctionError } = require('../commons-errors');
const Util = require('../commons-util');
const uuidv1 = require('uuid/v1');

class Function extends AbstractOperation {

    constructor(options, context) {
        super(uuidv1(), context);
        this.type = options.type;
        this.name = options.functionName;
        this.dir = options.dir;
        this.function = FunctionFactory.createFunctionByName(options);
        this.sourceCode = this.function.handle.toString();
    };

    //Flow
    doExecute(options, callbackAbstractOperation) {

        Util.info(`Execute function name=${this.name}`, {
            uuid: this.uuid,
            flowStepUuid: options.flowStep.uuid,
            contextUuid: options.context.uuid,
            sourceCode: Util.stringifyInfo(this.sourceCode)
        });

        try {
            this.handleFunctionFlow(options.context, callbackAbstractOperation);
        } catch (error) {
            Util.error(`Unexpected error to execute function flow name=${this.name}`, error);
            this.handleFunctionError(error, callbackAbstractOperation);
        };

    };

    //Function part of Flow
    doExecuteFunction(options, context, callbackAbstractOperation) {

        Util.info(`Execute operation function name=${this.name}`, {
            uuid: this.uuid,
            contextUuid: context.uuid,
            sourceCode: Util.stringifyInfo(this.sourceCode)
        });

        try {
            this.handleFunction(options, context, callbackAbstractOperation);
        } catch (error) {
            Util.error(`Unexpected error to execute function name=${this.name}`, error);
            this.handleFunctionError(error, callbackAbstractOperation);
        };

    };

    //Function part of Flow
    doExecuteFunctionWithoutCallback(options, context) {

        Util.info(`Execute operation function name=${this.name}`, {
            uuid: this.uuid,
            contextUuid: context.uuid,
            sourceCode: Util.stringifyInfo(this.sourceCode)
        });

        try {
            this.function.handle(options, context);
        } catch (error) {
            Util.error(`Unexpected error to execute function name=${this.name}`, error);
        };

    };

    handleFunctionFlow(context, callbackAbstractOperation) {

        let result = this.function.handle(context);

        this.validateResponse(result, callbackAbstractOperation);

    };

    handleFunction(options, context, callbackAbstractOperation) {

        let result = this.function.handle(options, context);

        this.validateResponse(result, callbackAbstractOperation);

    };

    validateResponse(result, callbackAbstractOperation) {
        switch (result.constructor) {
            case Promise:
                result.then((resultPromisse) => {
                    this.statusCode = Const.HTTP_STATUS.OK;
                    this.error = false;
                    this.setResult(resultPromisse || {});
                    callbackAbstractOperation(null, resultPromisse);
                }).catch((errorPromisse) => {
                    let objectError = new CreateFunctionError(errorPromisse);
                    this.statusCode = objectError.responseCode || objectError.codigoResposta;
                    this.error = true;
                    this.setResult(Util.tryParseJSON(objectError));
                    callbackAbstractOperation(errorPromisse, null);
                });
                break;
            case Object:
                this.statusCode = Const.HTTP_STATUS.OK;
                this.error = false;
                this.setResult(result || {});
                callbackAbstractOperation(null, result);
                break;
            case String:
                this.statusCode = Const.HTTP_STATUS.OK;
                this.error = false;
                this.setResult(result);
                callbackAbstractOperation(null, result);
                break;
            default:
                callbackAbstractOperation(null, result);
                break;
        };
    }

    handleFunctionError(error, callbackAbstractOperation) {
        let objectError = new CreateFunctionError(error);
        this.statusCode = objectError.responseCode || objectError.codigoResposta;
        this.error = true;
        this.setResult(Util.tryParseJSON(objectError));
        callbackAbstractOperation(objectError, null);
    }

};

module.exports = Function;