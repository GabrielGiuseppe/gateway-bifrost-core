'use strict';

const fs = require('fs');
const _ = require('underscore');
const AbstractOperation = require('./abstract-operation');
const uuidv1 = require('uuid/v1');
const Util = require('../commons-util');

class Mock extends AbstractOperation {

    constructor(options, flowContext) {

        super(uuidv1(), flowContext);

        if (!options.file) throw "The attribute 'file' is not defined in mock operation";

        this.type = options.type;
        this.description = options.description || '';
        this.name = options.file;
        this.file = `${process.cwd()}/mocks/${options.file}`;

    }

    doExecute(options, callback) {

        Util.info(`Execute mock file=${this.name}`, {
            uuid: this.uuid,
            flowStepUuid: options.flowStep.uuid,
            contextUuid: options.context.uuid
        });

        fs.readFile(_.template(this.file)(options.context),
            (error, result) => {
                callback(error, JSON.parse(result));
            });

    };

};

module.exports = Mock;