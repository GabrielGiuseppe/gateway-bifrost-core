'use strict';

const Util = require('../commons-util');
const Env = require('../environment').application;
const Step = require('step');
const _ = require('underscore');

const ClientFactory = require('../factory/client-factory');
const Const = require('../commons-const');
const { Errors } = require('../commons-errors');
const AbstractOperation = require('./abstract-operation');
const uuidv1 = require('uuid/v1');
const TransformationFunctionFactory = require('../factory/transformation-function-factory');

_.templateSettings = {
    interpolate: /\{\{(.+?)\}\}/g
};

class Http extends AbstractOperation {

    constructor(options, flowContext) {

        super(uuidv1(), flowContext);

        this.type = options.type || Const.FLOW_OPERATION_TYPE.HTTP;
        this.path = options.path || '';
        this.method = options.method || Const.HTTP_METHOD.GET;
        this.body = options.body || '';
        this.mergedHeaders = options.mergedHeaders || false;
        this.httpClient = ClientFactory.createByName(options.clientName);
        this.headers = options.headers || {};
        this.name = options.clientName;
        this.headersTransformation = TransformationFunctionFactory.create(options.headersTransformation, this);
        this.requestTransformation = TransformationFunctionFactory.create(options.requestTransformation, this);
        this.responseTransformation = TransformationFunctionFactory.create(options.responseTransformation, this);
        this.ifResponseError = TransformationFunctionFactory.create(options.ifResponseError, this);
        this.onError = TransformationFunctionFactory.create(options.onError, this);
        this.onSuccess = TransformationFunctionFactory.create(options.onSuccess, this);

    };

    doExecute(options, callbackAbstractOperation) {

        let self = this;

        let context = options.context;

        let optionsInvoke = {
            path: this.compilePath(this.path, context),
            method: this.method,
            query: context.query,
            body: this.compileBody(context.body, context),
            params: context.params,
            headers: this.headers,
            uuid: this.uuid,
            flowStepUuid: options.flowStep.uuid,
            contextUuid: options.context.uuid,
            name: this.name,
            type: this.type.toUpperCase()
        };

        Util.info(`Options (Before Transformation) operation flow(${optionsInvoke.type}) name=${optionsInvoke.name}`, {
            uuid: optionsInvoke.uuid,
            flowStepUuid: optionsInvoke.flowStepUuid,
            contextUuid: optionsInvoke.contextUuid,
            method: optionsInvoke.method,
            path: optionsInvoke.path,
            body: Util.stringifyInfo(optionsInvoke.body || {}),
            form: Util.stringifyInfo(optionsInvoke.form || {}),
            query: Util.stringifyInfo(optionsInvoke.qs || {}),
            headers: Util.stringifyInfo(optionsInvoke.headers)
        });

        Step(
            function () {
                self.prepareHeaders(optionsInvoke, context, this);
            },
            function (error) {
                Util.throwErrorIfItExists(error);
                self.headersTransformation.doExecuteFunction(optionsInvoke.headers, context, this);
            },
            function (error, headersTransformed) {
                Util.throwErrorIfItExists(error);
                //CLONAR PARA OBRIGAR A FUNCTION RETORNAR O VALOR
                optionsInvoke.headers = headersTransformed;
                self.requestTransformation.doExecuteFunction(_.clone(optionsInvoke), context, this);
            },
            function (error, optionsInvokeTransformed) {
                Util.throwErrorIfItExists(error);
                self.invokeService(optionsInvokeTransformed, this);
            },
            function (error, response) {
                self.handleHttpResponse(error, response, this);
            },
            function (error, result) {
                if (error) {
                    self.ifResponseError.doExecuteFunctionWithoutCallback(error, context);
                    self.onError.doExecuteFunctionWithoutCallback(error, context);
                } else {
                    self.onSuccess.doExecuteFunctionWithoutCallback(result, context);
                };
                Util.throwErrorIfItExists(error);
                self.responseTransformation.doExecuteFunction(result, context, this);
            },
            function (error, result) {
                callbackAbstractOperation(error, result);
            });

    };

    invokeService(optionsInvoke, callbackStep) {

        Util.info(`Options (Before Transformation) operation flow(${optionsInvoke.type}) name=${optionsInvoke.name}`, {
            uuid: optionsInvoke.uuid,
            flowStepUuid: optionsInvoke.flowStepUuid,
            contextUuid: optionsInvoke.contextUuid,
            method: optionsInvoke.method,
            path: optionsInvoke.path,
            body: Util.stringifyInfo(optionsInvoke.body || {}),
            form: Util.stringifyInfo(optionsInvoke.form || {}),
            query: Util.stringifyInfo(optionsInvoke.qs || {}),
            headers: Util.stringifyInfo(optionsInvoke.headers)
        });

        this.httpClient.execute(optionsInvoke, callbackStep);

    };

    handleHttpResponse(error, response, callbackStep) {

        let responseHeaders = (response && response.hasOwnProperty('headers')) ? response.headers : {};

        if (error) {
            let objectError = Errors.handleError(Util.parseJSON(error || {}));
            this.statusCode = objectError.responseCode || objectError.codigoResposta;
            this.responseHeaders = responseHeaders;
            this.error = true;
            this.setResult(Util.tryParseJSON(objectError));
            callbackStep(error, null);
        } else {
            this.statusCode = response.statusCode;
            this.error = false;
            this.responseHeaders = responseHeaders;
            this.setResult(Util.tryParseJSON(response.body));
            callbackStep(null, this.getResult());
        };

    };

    handleResponse(error, result, flowStep, callbackFromOrchestrationFlow) {

        if (error) {
            let objectError = Errors.handleError(Util.parseJSON(error || {}));
            flowStep.statusCode = objectError.responseCode || objectError.codigoResposta;
            flowStep.error = true;
            flowStep.responseHeaders = this.responseHeaders;
            flowStep.setResult(Util.tryParseJSON(objectError));
            callbackFromOrchestrationFlow(objectError, null);
        } else {
            flowStep.statusCode = Const.HTTP_STATUS.OK;
            flowStep.error = false
            flowStep.responseHeaders = this.responseHeaders;
            flowStep.setResult(Util.tryParseJSON(result));
            callbackFromOrchestrationFlow(null, Util.tryParseJSON(result));
        };

    };

    prepareHeaders(optionsInvoke, context, callbackStep) {

        try {

            optionsInvoke.headers = this.compileHeaders(optionsInvoke.headers, context);

            optionsInvoke.headers = _.extend(optionsInvoke.headers, {
                [Const.HEADERS.X_REQUEST_UUID_NAME]: context.uuid
            });
            
            if(context.headers[Const.HEADERS.X_WHITE_LABEL_UUID_NAME]){
                optionsInvoke.headers = _.extend(optionsInvoke.headers, {
                    [Const.HEADERS.X_WHITE_LABEL_UUID_NAME]: context.headers[Const.HEADERS.X_WHITE_LABEL_UUID_NAME]
                });
            }

            if (context.security.customerUUID) {
                optionsInvoke.headers = _.extend(optionsInvoke.headers, {
                    [Const.HEADERS.X_CUSTOMER_UUID_NAME]: context.security.customerUUID
                });
            };

            if (context.security.userData) {
                optionsInvoke.headers = _.extend(optionsInvoke.headers, {
                    [Const.HEADERS.X_USER_UUID_NAME]: context.security.userData.uuid
                });
            };

            if (context.headers[Const.HEADERS.X_ENTITY_UUID_NAME]) {
                optionsInvoke.headers = _.extend(optionsInvoke.headers, {
                    [Const.HEADERS.X_ENTITY_UUID_NAME]: context.headers[Const.HEADERS.X_ENTITY_UUID_NAME]
                });
            }

            if (context.headers[Const.HEADERS.X_CLIENT_UUID_NAME]) {
                optionsInvoke.headers = _.extend(optionsInvoke.headers, {
                    [Const.HEADERS.X_CLIENT_UUID_NAME]: context.headers[Const.HEADERS.X_CLIENT_UUID_NAME]
                });
            }

            if (context.headers[Const.HEADERS.X_EXERCISE_NAME]) {
                optionsInvoke.headers = _.extend(optionsInvoke.headers, {
                    [Const.HEADERS.X_EXERCISE_NAME]: context.headers[Const.HEADERS.X_EXERCISE_NAME]
                });
            }

            callbackStep();

        } catch (error) {

            Util.error(`Error to prepare headers=${optionsInvoke.headers}, error=${error}`);
            callbackStep(error);

        };

    };

    compileBody(body, context) {

        let bodyCompiled = body;

        if (this.body.indexOf('{{') !== -1) {
            try {
                let compiled = _.template(this.body);
                bodyCompiled = compiled(this.getCompileContext(context));
                Util.info('Compile Body', {
                    body: Util.stringifyInfo(body),
                    bodyCompiled: Util.stringifyInfo(bodyCompiled)
                });
            } catch (error) {
                Util.error(`Error to compile body=${body}, error=${error}`);
            }
        }

        return bodyCompiled;

    };

    compilePath(path, context) {

        let pathCompiled = _.clone(path);

        if (path.indexOf('{{') !== -1) {
            try {
                let compiled = _.template(path);
                pathCompiled = compiled(this.getCompileContext(context));
                Util.info('Compile path', {
                    path: path,
                    pathCompiled: pathCompiled
                });
            } catch (error) {
                Util.error(`Error to compile path=${path}, error=${error}`);
            }
        }

        return pathCompiled;

    };

    getCompileContext(context) {
        return _.extend(context, {
            env: Env
        });
    };

    compileHeaders(headers, context) {

        let compiledHeaders = _.clone(headers);

        for (let index in headers) {
            let header = headers[index];
            if (header && header.indexOf('{{') !== -1) {
                try {
                    let compiled = _.template(header);
                    compiledHeaders[index] = compiled(this.getCompileContext(context));
                    Util.info('Compile header', {
                        header: header,
                        compiledHeader: compiledHeaders[index]
                    });
                } catch (error) {
                    Util.error(`Error to compile header=${header}, error=${error}`);
                    return compiledHeaders
                }
            }
        }

        if (this.mergedHeaders === true)
            compiledHeaders = _.extend(compiledHeaders, context.headers);

        let requestCookie = context.headers[Const.HEADERS.COOKIE.toLocaleLowerCase()] || {};

        if (Util.nonNull(requestCookie)) {
            compiledHeaders = _.extend(compiledHeaders, {
                [Const.HEADERS.COOKIE.toLocaleLowerCase()]: requestCookie
            });
        };

        return compiledHeaders;

    };

};

module.exports = Http;