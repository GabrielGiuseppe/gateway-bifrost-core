'use strict';

const Util = require('../commons-util');
const Step = require('step');
const _ = require('underscore');
const ClientFactory = require('../factory/client-factory');
const Const = require('../commons-const');
const { Errors } = require('../commons-errors');
const AbstractOperation = require('./abstract-operation');
const uuidv1 = require('uuid/v1');

_.templateSettings = {
    interpolate: /\{\{(.+?)\}\}/g
};

class MonitorCheck extends AbstractOperation {

    constructor(options, flowContext) {

        super(uuidv1(), flowContext);

        this.type = options.type;
        this.path = options.path || '';
        this.method = options.method || Const.HTTP_METHOD.GET;
        this.httpClient = ClientFactory.createByName(options.clientName);
        this.headers = options.headers || {};
        this.name = options.clientName;

    };

    doExecute(options, callbackAbstractOperation) {

        let self = this;

        let context = options.context;

        let optionsInvoke = {
            path: this.compilePath(this.path, context),
            method: this.method,
            query: context.query,
            body: context.body,
            params: context.params,
            headers: this.compileHeaders(this.headers, context),
            monitor: true
        };

        Step(
            function () {
                self.invokeService(optionsInvoke, options, this);
            },
            function (error, response) {
                self.handleHttpResponse(error, response, this);
            },
            function (error, result) {
                callbackAbstractOperation(error, result);
            });

    };

    invokeService(optionsInvoke, options, callbackStep) {

        Util.info(`Request operation flow(${this.type.toUpperCase()}) name=${this.name}`, {
            flowStepUuid: options.flowStep.uuid,
            contextUuid: options.context.uuid,
            method: optionsInvoke.method,
            path: optionsInvoke.path,
            body: Util.stringifyInfo(optionsInvoke.body || {}),
            form: Util.stringifyInfo(optionsInvoke.form || {}),
            query: Util.stringifyInfo(optionsInvoke.qs || {}),
            headers: Util.stringifyInfo(optionsInvoke.headers),
            timeout: optionsInvoke.timeout
        });

        this.httpClient.execute(optionsInvoke, callbackStep);

    };

    handleHttpResponse(error, response, callbackStep) {

        let responseHeaders = (response && response.hasOwnProperty('headers')) ? response.headers : {};

        if (error) {
            let objectError = Errors.handleError(Util.parseJSON(error || {}));
            this.statusCode = objectError.responseCode || objectError.codigoResposta;
            this.responseHeaders = responseHeaders;
            this.error = true;
            this.setResult(Util.tryParseJSON(objectError));
            callbackStep(error, null);
        } else {
            this.statusCode = response.statusCode;
            this.error = false;
            this.responseHeaders = responseHeaders;
            this.setResult(Util.tryParseJSON(response.body));
            callbackStep(null, this.getResult());
        };

    };

    handleResponse(error, result, flowStep, callbackFromOrchestrationFlow) {

        if (error) {
            let objectError = Errors.handleError(Util.parseJSON(error || {}));
            flowStep.statusCode = objectError.responseCode || objectError.codigoResposta;
            flowStep.error = true;
            flowStep.responseHeaders = this.responseHeaders;
            flowStep.setResult(Util.tryParseJSON(objectError));
            callbackFromOrchestrationFlow(objectError, null);
        } else {
            flowStep.statusCode = Const.HTTP_STATUS.OK;
            flowStep.error = false
            flowStep.responseHeaders = this.responseHeaders;
            flowStep.setResult(Util.tryParseJSON(result));
            callbackFromOrchestrationFlow(null, Util.tryParseJSON(result));
        };

    };

    compilePath(path, context) {

        let pathCompiled = _.clone(path);

        if (path.indexOf('{{') !== -1) {
            try {
                let compiled = _.template(path);
                pathCompiled = compiled(context);
                Util.info('Compile path', {
                    path: path,
                    pathCompiled: pathCompiled
                });
            } catch (error) {
                Util.error(`Error to compile path=${path}, error=${error}`);
            }
        }

        return pathCompiled;

    };

    compileHeaders(headers, context) {

        let compiledHeaders = _.clone(headers);

        for (let index in headers) {
            let header = headers[index];
            if (header && header.indexOf('{{') !== -1) {
                try {
                    let compiled = _.template(header);
                    compiledHeaders[index] = compiled(context);
                    Util.info('Compile header', {
                        header: header,
                        compiledHeader: compiledHeaders[index]
                    });
                } catch (error) {
                    Util.error(`Error to compile header=${header}, error=${error}`);
                    return compiledHeaders
                }
            }
        };

        return compiledHeaders;

    };

};

module.exports = MonitorCheck;