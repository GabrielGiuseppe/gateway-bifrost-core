'use strict';

class ServiceGroup {

    constructor(options) {
        this.description = options.description;
        this.basePath = options.basePath;
        this.services = options.services;
        this.version = options.version;
    };

}

module.exports = ServiceGroup;