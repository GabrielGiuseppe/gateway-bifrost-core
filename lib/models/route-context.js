'use strict';

const uuidv1 = require('uuid/v1');
const _ = require('underscore');
const SecurityRoute = require('./security-route');
const Const = require('../commons-const');
const BaseContext = require('./base-context');
const ServiceContext = require('./service-context');

class RouteContext extends BaseContext {

    //Request e response devem manter a referencia
    constructor(request, response, next, serviceOptions, serviceGroup, orchestrator) {
        super(uuidv1(), {});
        this.next = next;
        this.orchestrator = orchestrator;
        this.service = new ServiceContext(serviceOptions, serviceGroup, this);
        this.statusCode = this.service.statusCode || Const.HTTP_STATUS.OK;
        this.security = new SecurityRoute();
        this.request = request;
        this.response = response;
        this.body = request.body || {};
        this.query = request.query || {};
        this.params = _.omit(request.params, Object.keys(request.query)) || {};
        this.headers = request.headers || {};
        this.attemptsIndex = 0;
        this.statusCode = Const.HTTP_STATUS.OK
    };

    setBody(body) {
        this.body = body;
    };

    getBody() {
        return this.body;
    };

    getHeaders() {
        this.headers;
    };

    setHeaders(headers) {
        _.extend(this.headers, headers);
    };

    getResponseHeaders() {
        this.responseHeaders;
    };

    setResponseHeaders(headers) {
        _.extend(this.responseHeaders, headers);
    };

    setParams(params) {
        _.extend(this.params, params);
    };

    setQuery(query) {
        _.extend(this.query, query);
    };

    getQuery() {
        return this.query;
    };

}

module.exports = RouteContext;