'use strict';

const FlowContext = require('./flow-context');
const BaseContext = require('./base-context');
const uuidv1 = require('uuid/v1');
const _ = require('underscore');

class ServiceContext extends BaseContext {

    constructor(options, group ,context) {
        super(uuidv1(), context);
        this.name = options.name || '';
        this.description = options.description || '';
        this.route = options.route || '';
        this.method = options.method.toLowerCase() || 'get';
        this.security = options.security || false;
        this.responseHeaders = options.responseHeaders || {};
        this.flow = createFlows(options.flow, this);
        this.previousFlow = {};
        this.currentFlow = {};
        this.indexedResult = options.indexedResult || false;
        this.headers = {};
        this.rawOptions = options;
        this.group = group || {};
    };

    setCurrentFlow(currentFlow){
        this.previousFlow = this.currentFlow;
        this.currentFlow = currentFlow;
    }

    getCurrentFlow(){
        return this.currentFlow;
    };

    getPreviousFlow(){
        return this.previousFlow;
    };

    getTotalFlow() {
        return this.flow.length;
    };

    addHeaders(headers){
        this.headers = _.extend(this.headers, headers);
    };

};

function createFlows(flows, context) {

    return flows.map(flow => {
        return new FlowContext(flow, context);
    });

};

module.exports = ServiceContext;