'use strict';

const Util = require('../commons-util');
const FlowIndexed = require('./flow-indexed');

class RequestIndexed {

    constructor(context) {
        this.uuid - context.uuid;
        this.request = {
            url: {
                protocol: context.request.isSecure() ? "https:" : "http:",
                hostname: context.request.headers.host,
                port: context.request.connection.localPort,
                path: context.request.getPath(),
                full: `${context.request.isSecure() ? "https:" : "http:"}//${context.request.headers.host}${context.request.getPath()}`
            },
            socket: {
                remoteAddress: context.request.socket.remoteAddress,
                encrypted: context.request.isSecure()
            },
            httpVersion: context.request.httpVersion,
            method: context.request.method,
            body: Util.stringifyInfo(context.request.body || {}),
            query: Util.stringifyInfo(context.query || {}),
            params: Util.stringifyInfo(context.params || {}),
            headers: context.headers || {}
        };
        this.service = {
            name: context.service.name,
            description: context.service.description,
            method: context.service.method,
            security: context.service.security,
            indexedResult: context.service.indexedResult,
            responseHeaders: Util.stringifyInfo(context.service.responseHeaders),
            totalFlows: context.service.flow.length,
        };

        if (context.service.indexedResult == true ||
            context.error == true) { 
            this.service.flows= this.generateFlow(context.service.flow, context.service.indexedResult);
        };

        let result = '';

        if (context.service.indexedResult == true ||
            context.error == true) {
            if (context.hasResult())
                result = Util.stringifyInfo(context.result);
        };

        this.response = {
            statusCode: context.response.statusCode,
            headers: Util.stringifyInfo(context.response.getHeaders()),
            result: result
        };

        this.timers = context.timers;
    };

    generateFlow(flows, indexedResult) {

        let objectFlow = {};

        flows.map((flow, index) => {
            objectFlow[index + 1] = new FlowIndexed(flow, indexedResult);
        });

        return objectFlow;

    }

};

module.exports = RequestIndexed;