'use strict';

class ServiceRoute {
    
    constructor(options) {
        //Validate path windows
        this.path = options.path.replace(/\\/g, "/");
        this.security = options.security || false;
        this.method = options.method.toLowerCase();
    }

    getPath() {
        return this.path;
    };

    getMethod() {
        return this.method;
    };
}

module.exports = ServiceRoute;