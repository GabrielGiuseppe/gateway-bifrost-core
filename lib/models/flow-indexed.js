'use strict';

const Util = require('../commons-util');
const Const = require('../commons-const');
const OperationHttpIndexed = require('./operation-http-indexed');
const OperationFunctionIndexed = require('./operation-function-indexed');
const OperationMockIndexed = require('./operation-mock-indexed');

class FlowIndexed {

    constructor(flow, indexedResult) {
        //DADOS GERAIS
        this.uuid = flow.uuid;
        this.hasError = flow.error;
        this.executed = flow.executed;
        this.statusCode = flow.statusCode;
        //COMPORTAMENTO
        this.behavior = {
            propagateResultToBody: flow.propagateResult,
            skipErrors: flow.skipErrors,
            resultVariable: flow.resultVariable,
            executeConditions: {
                condition: flow.condition,
                validation: flow.validation
            }
        };
        //CACHE
        this.configCache = {
            enable: flow.cacheEnable,
            key: flow.cacheKey || '',
            name: flow.cacheName || '',
            ttl: flow.cacheTtl
        };
        //OPERACAO DO FLOW
        this.generateType(flow.operation, indexedResult);
        //RESULTADO
        this.result = indexedResult ? Util.stringifyInfo(flow.result) : '';
        //TIMERS
        this.duration = flow.timers.duration
    };

    generateType(operation, indexedResult) {

        switch (operation.type) {
            case Const.FLOW_OPERATION_TYPE.HTTP:
                this.operationHttp = new OperationHttpIndexed(operation, indexedResult);
                break
            case Const.FLOW_OPERATION_TYPE.FUNCTION:
                this.operationFunction = new OperationFunctionIndexed(operation, indexedResult);
                break
            case Const.FLOW_OPERATION_TYPE.MOCK:
                this.operationMock = new OperationMockIndexed(operation, indexedResult);
                break
            default:
                break;
        };

    };

};

module.exports = FlowIndexed;