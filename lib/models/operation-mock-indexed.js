'use strict';

const Util = require('../commons-util');

class OperationMockIndexed {

    constructor(operation, indexedResult) {
        //DADOS GERAIS
        this.uuid = operation.uuid;
        this.description = operation.description || '';
        this.type = operation.type;
        this.file = operation.file;
        this.hasError = operation.error;
        this.executed = operation.executed;
        this.statusCode = operation.statusCode;
        this.result = (indexedResult || operation.error) ? Util.stringifyInfo(operation.result) : '';
        this.duration = operation.timers.duration
    };

};

module.exports = OperationMockIndexed;