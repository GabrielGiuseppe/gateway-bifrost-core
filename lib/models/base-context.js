'use strict';

const Const = require('../commons-const');
const { performance } = require('perf_hooks');
const Util = require('../commons-util');
const _ = require('underscore')

class BaseContext {

    constructor(uuid, context) {
        this.uuid = uuid;
        this.context = context || {};
        this.statusCode = Const.HTTP_STATUS.OK;
        //InfoTime
        this.timers = {
            created: {
                at: new Date().toISOString(),
                ms: performance.now()
            },
            started: {
                at: Util.defaultDateISOString(),
                ms: 0
            },
            finished: {
                at: Util.defaultDateISOString(),
                ms: 0
            },
            duration: 0
        };
        this.error = false;
        this.executed = false;
        //Result (Error|Success)
        this.inputData = {};
        this.responseHeaders = {};
        this.result = {};
    };

    setContext(context) {
        this.context = context;
    };

    init() {
        this.timers.started.ms = performance.now();
        this.timers.started.at = new Date().toISOString();
    };

    end() {
        this.executed = true;
        this.timers.finished.ms = performance.now();
        this.timers.finished.at = new Date().toISOString();
        this.timers.duration = parseFloat((this.timers.finished.ms - this.timers.started.ms).toFixed(4));
    };

    getResult() {
        return this.result;
    };

    //Return JSON to Client
    getResultJSON() {
        return Util.stringify(this.result);
    }

    setResult(result) {
        this.result = result || {};
    };

    addResult(result) {
        this.result = _.extend(this.result, result);
    };

    hasResult() {
        return !(Object.keys(this.result).length === 0 && this.result.constructor === Object);
    };

}

module.exports = BaseContext;