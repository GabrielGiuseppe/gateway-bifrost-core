'use strict';

const OperationFlowFactory = require('../factory/operation-flow-factory');
const BaseContext = require('./base-context');
const uuidv1 = require('uuid/v1');
const Const = require('../commons-const');

class FlowContext extends BaseContext {

    constructor(options, context) {

        super(uuidv1(), context);
        this.resultVariable = options.resultVariable || '.';
        this.skipErrors = options.skipErrors || false;
        this.propagateResult = options.propagateResult || false;
        this.cacheEnable = options.cacheEnable || false;

        this.condition = this.generateCondition(options);

        this.validation = options.validation || "true";
        this.cacheTtl = options.cacheTtl || 0;
        this.cacheName = options.cacheName || '';
        this.cacheKey = options.cacheKey || '';
        this.operation = OperationFlowFactory.createOperationByType(options.operation, this);
    };

    generateCondition(options) {

        let condition = (options.condition || '');

        if (condition.constructor.name == Const.OBJECT_TYPE.OBJECT) {

            condition.whenFail = condition.whenFail || {};

            return {
                compileExpression: options.condition.compileExpression || "true",
                whenFail: {
                    responseHeaders: options.condition.whenFail.responseHeaders || {},
                    error: options.condition.whenFail.error || false,
                    result: options.condition.whenFail.result || {},
                    statusCode: options.condition.whenFail.statusCode || 200
                }
            };
        };

        return {
            compileExpression: condition || "true",
            whenFail: {
                responseHeaders: {},
                error: false,
                result: {},
                statusCode: 200
            }
        };

    }

};

module.exports = FlowContext;