'use strict';

const Util = require('../commons-util');
const Const = require('../commons-const');
const OperationFuncTransIndexed = require('./operation-function-transformation-indexed');

class OperationHttpIndexed {

    constructor(operation, indexedResult) {
        //DADOS GERAIS
        this.uuid = operation.uuid;
        this.name = operation.name;
        this.type = operation.type;
        this.method = operation.method;
        this.path = operation.path;
        this.hasError = operation.error;
        this.executed = operation.executed;
        this.statusCode = operation.statusCode;
        this.headers = operation.headers
        this.mergedHeaders = operation.mergedHeaders;
        this.httpConfigClient = Util.stringifyInfo(operation.httpClient);

        this.functions = {
            headersTransformation: new OperationFuncTransIndexed(operation.headersTransformation, indexedResult),
            requestTransformation: new OperationFuncTransIndexed(operation.requestTransformation, indexedResult),
            responseTransformation: new OperationFuncTransIndexed(operation.responseTransformation, indexedResult),
            onError: new OperationFuncTransIndexed(operation.onError, indexedResult),
            onSuccess: new OperationFuncTransIndexed(operation.onSuccess, indexedResult),
        };

        this.result = (indexedResult || operation.error) ? Util.stringifyInfo(operation.result) : '';
        this.duration = operation.timers.duration

    };

};

module.exports = OperationHttpIndexed;