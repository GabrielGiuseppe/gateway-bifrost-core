'use strict';

const Util = require('../commons-util');

class OperationFunctionTransformationIndexed {

    constructor(operation, indexedResult) {
        //DADOS GERAIS
        this.uuid = operation.uuid;
        this.name = operation.name;
        this.type = operation.type;
        this.sourceCode = operation.sourceCode;
        this.hasError = operation.error;
        this.executed = operation.executed;
        this.statusCode = operation.statusCode;
        this.inputData = indexedResult ? Util.stringifyInfo(operation.inputData) : '';
        this.result = (indexedResult || operation.error) ? Util.stringifyInfo(operation.result) : '';
        this.duration = operation.timers.duration
    };

};

module.exports = OperationFunctionTransformationIndexed;