'use strict';

class SecurityRoute {
    constructor() {
        this.encryptionKey = null;
        this.publicKey = null;
        this.customerUUID = null;
        this.devicePublicKeyDisgest = null;
        this.accessToken = null;
        this.deviceDNA = null;

    }

    setCustomerUUID(customerUUID) {
        this.customerUUID = customerUUID;
    };

    setEncryptionKey(encryptionKey) {
        this.encryptionKey = encryptionKey;
    };

    setPublicKey(publicKey) {
        this.publicKey = publicKey;
    };

    setDevicePublicKeyDisgest(devicePublicKeyDisgest) {
        this.devicePublicKeyDisgest = devicePublicKeyDisgest;
    };

    setAccessToken(accessToken) {
        this.accessToken = accessToken;
    };

    setDeviceDNA(deviceDNA) {
        this.deviceDNA = deviceDNA;
    };

}


module.exports = SecurityRoute;