'use strict';

var util = require('./commons-util');
var morgan = require('morgan');
var environment = require('./environment');
var errors = require('restify-errors');
var xss = require('xss-clean');

const ALLOW_HEADERS_DEFAULT = ['Origin', 'Authorization', 'Content-Type', 'Accept', 'Accept-Version', 'Api-Version', 'X-Access-Token', 'X-Device-Public-Key', 'x-permission', 'X-Permission', 'x-encryption', 'X-Encryption', 'x-validate', 'X-Validate'];

module.exports = function (server, restify) {

    util.info("Load default configurations");

    server.app.pre(restify.pre.sanitizePath()); 
    server.app.use(restify.plugins.acceptParser(server.app.acceptable));
    server.app.use(restify.plugins.queryParser());
    server.app.use(restify.plugins.bodyParser());
    server.app.use(restify.plugins.gzipResponse());
    server.app.use(morgan('dev'));

    /*server.app.use(function setETag(req, res, next) {
        res.header('ETag', 'myETag');
        res.header('Last-Modified', new Date());
    });

    server.app.use(restify.conditionalRequest());

    server.app.use(restify.requestExpiry({
        header: 'x-request-expiry-time'
    }));*/

    // Extend logger using the plugin.
    // server.app.use(restify.requestLogger());

    server.app.use(xss());

    server.app.use(restify.plugins.throttle({
        burst: environment.application.requestPerSecond, // Max 10 concurrent requests
        rate: (environment.application.requestPerSecond / 2), //Steady state number of requests/second to allow
        ip: true
    }));

    server.app.use(
        (req, res, next) => {
            res.header('Access-Control-Allow-Headers', ALLOW_HEADERS_DEFAULT.join(', '));
            res.header('Access-Control-Allow-Methods', req.method);
            res.header('Access-Control-Allow-Origin', req.headers.origin);
            res.header('Access-Control-Allow-Credentials', true);
            res.header('X-Content-Type-Options', 'nosniff');
            res.header('X-Frame-Options', 'DENY');
            res.header('Pragma', 'no-cache');
            res.header('Expires', '0');
            res.header('Connection', 'close');
            res.header('Accept-Encoding', 'gzip');
            res.header('Cache-Control', 'no-cache, no-store, max-age=0, must-revalidate');
            res.header('X-XSS-Protection', 'mode=block');
            return next();
        }
    );

    server.app.use((req, res, next) => {
        // Removing the keep alive to not reuse connections on the vip.
        res.shouldKeepAlive = true;
        next();
    });

    server.app.get("/healthcheck", (req, res) => {
        res.send(200, {
            success: true
        });
    });

    server.app.get("/monitor/health", (req, res) => {
        res.send(200, {
            status: "UP"
        });
    });

    server.app.get("/monitor/info", (req, res) => {
        res.send(200, {
            application: {
                name: environment.application.appName,
                version: environment.application.version
            }
        });
    });

    server.app.get("/", (req, res) => {
        res.send(200, {
            success: true
        });
    });

    //lets-encrypt
    server.app.get("/.well-known/.*", (req, res, next) => {
        res.write(environment.application.acmeChallenge);
        res.end();
        next();
    });

    function unknownMethodHandler(req, res) {

        var requestedMethod = req.headers['access-control-request-method']

        if (req.method.toLowerCase() === 'options' && requestedMethod) {

            res.header('Access-Control-Allow-Methods', [req.method, requestedMethod]);
            res.header('Access-Control-Allow-Origin', req.headers.origin);
            res.header('Access-Control-Allow-Credentials', true);
            res.header('Access-Control-Allow-Headers', ALLOW_HEADERS_DEFAULT.join(', '));

            return res.send(200);
            
        } else
            return res.send(new errors.MethodNotAllowedError());
    }

    server.app.on('MethodNotAllowed', unknownMethodHandler);

    server.app.on('NotFound', (req, res) => {
        return res.send(new errors.ResourceNotFoundError("Invalid URI"));
    });

};
