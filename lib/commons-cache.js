'use strict';

const Util = require('./commons-util');
const Step = require("step");
const redisClient = require("redis");

const DEFAULT_REDIS_CONNECT_TIMEOUT = 30000;
const DEFAULT_REDIS_RECONNECT_TIMEOUT = 30000;

class RedisClient {

    constructor(options) {

        this.enableRedis = options.hasOwnProperty('enableRedis') ? options.enableRedis : false;
        this.readClient = null;
        this.writerClient = null;

        if (this.enableRedis) {

            let hasConfig = options.hasOwnProperty('redisCache');

            if (hasConfig) {

                let redisCache = options.redisCache;

                this.readClient =
                    redisClient.createClient(redisCache.reader.port, redisCache.reader.host, {
                        no_ready_check: true,
                        connect_timeout: redisCache.reader.connectTimeout || DEFAULT_REDIS_CONNECT_TIMEOUT,
                        retry_strategy: retryStrategy
                    }).on("error", function (err) {
                        Util.error(`${err} to connect Redis[read]`);
                    }).on('connect', function () {
                        Util.info('Redis[read] connected',
                            {
                                host: redisCache.reader.host,
                                port: redisCache.reader.port
                            });
                    });

                this.writerClient =
                    redisClient.createClient(redisCache.writer.port, redisCache.writer.host, {
                        no_ready_check: true,
                        connect_timeout: redisCache.writer.connectTimeout || DEFAULT_REDIS_CONNECT_TIMEOUT,
                        retry_strategy: retryStrategy
                    }).on("error", function (err) {
                        Util.error(`${err} to connect Redis[writer]`);
                    }).on('connect', function () {
                        Util.info('Redis[writer] connected',
                            {
                                host: redisCache.writer.host,
                                port: redisCache.writer.port
                            });
                    });
            };
        };
    };

    put(cacheName, key, value, ttl) {

        Util.info('Put Object in Cache',
            {
                cacheName: Util.stringify(cacheName),
                cacheKey: Util.stringify(key),
                ttl: Util.stringify(ttl),
                value: Util.stringify(value)
            });

        if (this.writerClient &&
            this.writerClient.connected) {
            let composedKey = this.composeKey(cacheName, key);
            this.writerClient.setex(composedKey, ttl, Util.stringify(value));
        }

    };

    get(cacheName, key, callback) {

        let self = this;

        if (this.readClient &&
            this.readClient.connected) {
            Step(
                function () {
                    let composedKey = self.composeKey(cacheName, key);
                    self.readClient.get(composedKey, this);
                },
                function (error, entry) {
                    // Never throws a error, a cache never stop a request.
                    // Util.throwErrorIfItExists(error);
                    if (entry) {
                        entry = JSON.parse(entry);
                        Util.info('Hit Object in Cache',
                            {
                                cacheName: Util.stringify(cacheName),
                                cacheKey: Util.stringify(key),
                                value: Util.stringify(entry)
                            });
                    } else {
                        Util.info('Miss Object in Cache',
                            {
                                cacheName: cacheName,
                                cacheKey: key
                            });
                    }

                    this(null, entry);
                },
                callback);
        } else {
            callback(null, null);
        }

    };

    async getAsync(key) {

        return new Promise((resolved, rejeted) => {

            if (this.readClient &&
                this.readClient.connected) {

                this.readClient.get(key, (error, entry) => {

                    if (entry) {
                        entry = JSON.parse(entry);
                        Util.info('Hit Object in Cache', { cacheKey: key });
                    } else {
                        Util.info('Miss Object in Cache', { cacheKey: key });
                    }

                    resolved(entry || {});

                });
            };

        });
    };

    getKeys(cacheName, callback) {

        let self = this;

        if (this.readClient &&
            this.readClient.connected) {
            Step(
                function () {
                    self.readClient.keys(cacheName + '*', this);
                },
                function (error, keys) {

                    if (keys) {

                        Util.info('Hit Object in Cache', { cacheName: cacheName });

                        Promise.all(keys.map(key => self.getAsync(key))).then(values => {
                            this(null, values);
                        });

                    } else {
                        Util.info('Miss Object in Cache', { cacheName: cacheName });
                    }

                },
                callback);
        } else {
            callback(null, null);
        }
    }

    remove(cacheName, key, callback) {

        if (this.writerClient &&
            this.writerClient.connected) {
            this.writerClient.del(this.composeKey(cacheName, key), callback);
        }

    };

    composeKey(cacheName, key) {
        return `${cacheName}:${key}`;
    };

};

function retryStrategy(options) {
    if (options.error && options.error.code === 'ECONNREFUSED') {
        // End reconnecting on a specific error and flush all commands with a individual error
        Util.error(`The server refused the connection error ${options.error.message}`);
        return DEFAULT_REDIS_RECONNECT_TIMEOUT;
    }
    if (options.total_retry_time > 1000 * 60 * 60) {
        // End reconnecting after a specific timeout and flush all commands with a individual error
        Util.error(`Retry time exhausted total time retry ${options.total_retry_time}`);
        return new Error(`Retry time exhausted total time retry ${options.total_retry_time}`);
    }
    // reconnect after
    return DEFAULT_REDIS_RECONNECT_TIMEOUT
};

exports.createClient = function (options) {
    return new RedisClient(options);
};