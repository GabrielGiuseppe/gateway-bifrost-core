'use strict';

var util = require('./commons-util');

var environment = (function Environment() {

    if (process.env.NODE_ENV === undefined) {
        process.env.NODE_ENV = 'development';
    };

    var _environment = process.env.NODE_ENV;

    var applicationConfig = require(process.cwd() + '/config/' + _environment + '-application');
    var mappingServices = require(process.cwd() + '/config/' + _environment + '-mapping-services');
    var mappingClients = require(process.cwd() + '/config/' + _environment + '-mapping-clients');

    var version = require(process.cwd() + '/package.json');

    applicationConfig.version = version.version;

    return {
        environment: _environment,
        application: applicationConfig,
        mappingServices: mappingServices,
        mappingClients: mappingClients
    };

})();

// Hook into commonJS module systems
module.exports = environment;