'use strict';

const Util = require('./commons-util');

const AWS_REGION_DEFAULT = 'us-east-1';
const HOST_DEFAULT = 'localhost:9200';

class ElasticSearch {

    constructor(options) {

        this.instance = null;

        this.enabled = options.hasOwnProperty('enableElasticSearch') ? options.enableElasticSearch : false;

        if (this.enabled) {

            let hasConfig = options.hasOwnProperty('elasticSearch');

            if (hasConfig) {

                let client = options.elasticSearch.hasOwnProperty('client') ? options.elasticSearch.client : 'pure';

                Util.info('Creating ElasticSearch Instance', Util.stringifyInfo(options.elasticSearch));

                let elasticSearch = options.elasticSearch;

                switch (client.toUpperCase()) {

                    case 'AWS':

                        let AWS = require('aws-sdk');
                        let credentials = elasticSearch.credentials;

                        AWS.config.update({
                            credentials: new AWS.Credentials(credentials.accessKeyId, credentials.secretAccessKey),
                            region: credentials.region || AWS_REGION_DEFAULT
                        });

                        this.instance = require('elasticsearch').Client({
                            hosts: [elasticSearch.host],
                            connectionClass: require('http-aws-es')
                        });

                        break;
                    default:
                        //PURE
                        this.instance = require('elasticsearch').Client({
                            host: elasticSearch.host || HOST_DEFAULT
                        });
                        break;
                }
            } else {
                this.enabled = false;
            };

        };

    };

    getInstance() {
        return this.instance;
    };

    isEnabled() {
        return this.enabled;
    };

}

exports.createClient = function (options) {
    return new ElasticSearch(options);
};