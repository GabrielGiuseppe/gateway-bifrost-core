'use strict';

var Util = require('./commons-util');
var environment = require('./environment');
var JWT = require('jsonwebtoken');
var { CreateCustomError, Errors } = require('./commons-errors');

function validateAccessToken(token, callback) {

    if (token) {

        JWT.verify(token, environment.application.security.secret, { algorithms: ['HS256'] }, function (err, decoded) {
            if (err) {
                Util.error(`Failed to authenticate token=${token}`, err);
                callback(Errors.handleSecurityError(err, null));
            } else {
                callback(null, decoded);
            }
        });

    } else {
        Util.error('Authentication token not informed.');
        callback(Errors.handleSecurityError(CreateCustomError('JsonWebTokenError', 'Authentication token not informed.')), null);
    }
}

function createAccessToken(payload) {

    var tokenJWT = JWT.sign(payload, environment.application.security.secret, {
        expiresIn: '365d' // 1 year
    });

    return tokenJWT;

};

exports.validateAccessToken = validateAccessToken;
exports.createAccessToken = createAccessToken;