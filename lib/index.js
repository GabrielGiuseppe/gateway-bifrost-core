'use strict'

const APIGateway = require('./factory/api-gateway');
const Environment = require('./environment');
const Util = require('./commons-util');
const JWTUtil = require('./jwt-util');
const Errors = require('./commons-errors');
const ClientFactory = require('./factory/client-factory');
const ElasticSearch = require('./commons-elasticsearch');
const Const = require('./commons-const');

if (process.env.NODE_ENV == 'production') {
    process.on('uncaughtException', function (err) {
        Util.error(`UncaughtException error=${Util.stringifyInfo(err)}`);
        console.error(err);
    });
};

process.on('SIGTERM', function () {
    Util.error(Util.format("Process %s was kill", process.pid));
    // some other closing procedures go here
    process.exit(0);
});

// Memory check:
process.nextTick(function memory() {
    Util.warning(Util.format('Memory (Master): %s', Math.round(((process.memoryUsage().rss / 1024) / 1024)) + "MB"));
    var timeout = setTimeout(memory, Environment.application.memoryCheckInterval);
});

// Hook into commonJS module systems
module.exports = {
    APIGateway: APIGateway,
    Environment: Environment,
    Const: Const,
    Util: Util,
    JWTUtil: JWTUtil,
    Errors: Errors,
    ClientFactory: ClientFactory,
    ElasticSearch: ElasticSearch
};