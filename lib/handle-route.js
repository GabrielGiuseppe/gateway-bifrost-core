'use strict';

const Util = require('./commons-util');
const Const = require('./commons-const');
const { ApiGatewayError } = require('./commons-errors');
const _ = require('underscore');
const Step = require('step');
const RouteContext = require('./models/route-context');
const ValidateSecurity = require('./security-validation');
const RequestIndexed = require('./models/request-indexed');
const Environment = require('./environment');
const UtilNode = require('util');

class HandleRoute {

    constructor(orchestrator, serviceOptions, serviceGroup, elasticSearch) {

        if (!serviceOptions.flow) {
            throw ApiGatewayError('You must provide the flow to be executed.');
        };

        this.orchestrator = orchestrator;
        this._serviceOptions = serviceOptions;
        this._serviceGroup = serviceGroup;
        this.elasticSearch = elasticSearch;

    };

    handleRequest(request, response, next) {

        let self = this;

        let context = new RouteContext(request, response, next, this._serviceOptions, this._serviceGroup, this.orchestrator);

        Util.info('Request', {
            method: context.service.method.toUpperCase(),
            service: context.service.name,
            url: context.request.url,
            uuid: context.uuid,
            body: context.body,
            query: context.query,
            params: context.params,
            headers: context.headers
        });

        //Inicia Route Request
        context.init();

        Step(
            //Valida se o Route tem segurança JWT
            //Todo: implementar funcao custom para validar autenticação
            function () {
                self.validateSecurity(context, this);
            },
            //Execute Flow
            function (error) {
                Util.throwErrorIfItExists(error);
                self.executeFlow(context, this);
            },
            function () {
                self.processResult(context);
            });

    };

    indexRequest(context) {

        if (this.elasticSearch.isEnabled()) {

            this.elasticSearch.getInstance().index({
                index: Environment.application.elasticSearch.indexName || Environment.application.appName,
                type: 'requests',
                id: context.uuid,
                body: new RequestIndexed(context)
            }, (err, data) => {
                //TENTATIVA DE INDEXAR NO ELASTICSEARCH
                context.attemptsIndex++;
                if (err) {
                    Util.error('Indexed Fail', { uuid: context.uuid, error: err });
                    //Retry Index
                    if (context.attemptsIndex < 10) {
                        Util.warning('Retry Index', {
                            uuid: context.uuid,
                            retryIn: "30s x " + context.attemptsIndex,
                            attemptsIndex: context.attemptsIndex
                        });
                        setTimeout(() => {
                            this.indexRequest(context);
                        }, 30000 * context.attemptsIndex);
                    };
                } else {
                    Util.info('Indexed Successfully', { uuid: context.uuid });
                };
            });
        };
    };

    validateSecurity(context, callback) {

        Step(
            function () {
                ValidateSecurity.execute(context, this);
            },
            function (error) {
                if (error) {
                    context.statusCode = error.responseCode;
                    context.error = true;
                    context.setResult(error);
                }
                callback(error);
            }
        );

    };

    executeFlow(context, callback) {

        Util.info(`Executing flow(s)`, {
            service: context.service.name,
            totalFlow: context.service.flow.length,
            contextUuid: context.uuid
        });

        this.orchestrator.execute(context, callback);
    };

    generateHeadersDefault(context) {

        let headers = _.extend({
            [Const.HEADERS.X_REQUEST_UUID_NAME]: context.uuid
        }, context.service.responseHeaders);

        let contentType = headers[Const.HEADERS.CONTENT_TYPE] || this.resolveContextTypeResponse(context);

        headers[Const.HEADERS.CONTENT_TYPE] = contentType;
        headers[Const.HEADERS.X_REQUEST_DURATION_NAME] = context.timers.duration;

        //VERIFY COOKIES ON RESPONSE HEADERS FROM MICROSERVICES
        let responseCookie = context.service.headers[Const.HEADERS.SET_COOKIE.toLocaleLowerCase()] || {};

        if (Util.nonNull(responseCookie)) {
            headers = _.extend(headers, {
                [Const.HEADERS.SET_COOKIE.toLocaleLowerCase()]: responseCookie
            });
        };

        //VERIFY CONTENT TYPE FOR DOWNLOAD FILE ON RESPONSE HEADERS FROM MICROSERVICES
        if (headers[Const.HEADERS.CONTENT_TYPE.toLocaleLowerCase()] == 'multipart/form-data' ||
            headers[Const.HEADERS.CONTENT_TYPE] == 'multipart/form-data') {

            let responseContentDisposition = context.service.headers[Const.HEADERS.CONTENT_DISPOSITION.toLocaleLowerCase()] || 'download_' + (new Date()).toDateString();

            headers = _.extend(headers, {
                [Const.HEADERS.CONTENT_DISPOSITION.toLocaleLowerCase()]: responseContentDisposition
            });
        };

        //Update Response Headers
        context.service.responseHeaders = headers;

        //NAO SETA HEADER PARA REDIRECTS E NO CONTENT PARA RESULTADOS VAZIOS
        if (!Util.isUrl(context.getResult())) {
            context.response.writeHead(Util.isEmpty(context.getResult()) ? Const.HTTP_STATUS.NO_CONTENT : context.statusCode, context.service.responseHeaders);
        };

    };

    resolveContextTypeResponse(context) {

        if (UtilNode.isBuffer(context.getResult()))
            return Const.STREAM_CONTENT_TYPE_VALUE;

        if (UtilNode.isObject(context.getResult()))
            return Const.JSON_CONTENT_TYPE_VALUE;

        if (UtilNode.isString(context.getResult()))
            return Const.DEFAULT_CONTENT_TYPE_VALUE;

        return Const.DEFAULT_CONTENT_TYPE_VALUE;

    };

    resolveEndResponse(context) {

        //Libera resposta para o client no formato correto
        switch (context.getResult().constructor.name) {
            case Const.OBJECT_TYPE.READSTREAM: //STREAM FOR BIG FILES
                context.getResult().pipe(context.response);
                break;
            case Const.OBJECT_TYPE.BUFFER: //BUFFER
                context.response.end(context.getResult())
                break;
            case Const.OBJECT_TYPE.OBJECT: //JSON
                context.response.end(Util.stringify(context.getResult()));
                break;
            case Const.OBJECT_TYPE.STRING: //STRING FOR HTML
                //VALIDA SE A RESPOSTA É UM URL VALIDA PARA REDIRECT
                if (Util.isUrl(context.getResult())) {
                    context.response.redirect(context.getResult(), context.next);
                } else {
                    context.response.end(context.getResult());
                };
                break;
            default: {
                try {
                    context.response.end(Util.stringify(context.getResult()));
                } catch (error) {
                    context.response.end(null);
                }
            }
        };

    };

    releaseResponse(context) {

        //TODO: Verificar como ver se tem erro para adicionar informações adicionais
        if (context.error) {
            context.setResult(_.extend(context.service.currentFlow.getResult() || {}, {
                //description: context.service.description,
                descricao: context.service.description,
                //serviceName: context.service.name,
                nomeServico: context.service.name,
                //requestId: context.uuid,
                idSolicitacao: context.uuid
            }));
        };

        this.resolveEndResponse(context);

    };

    processResult(context) {

        this.handleResult(context);
        this.indexRequest(context);

    };

    handleResult(context) {

        //Finish Request
        context.end();

        this.generateHeadersDefault(context);
        this.releaseResponse(context);

        Util.info('Response', {
            method: context.service.method.toUpperCase(),
            service: context.service.name,
            route: context.request.url,
            uuid: context.uuid,
            body: Util.stringifyInfo(context.getResultJSON()),
            headers: context.service.responseHeaders,
            duration: context.timers.duration
        });

    };

};

module.exports = HandleRoute;