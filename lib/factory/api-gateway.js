'use strict';

var ServerFactory = require('./server-factory')

function createServer(options) {
    return ServerFactory.createServer(options);
};

// Hook into commonJS module systems
module.exports = {
    createServer: createServer
};