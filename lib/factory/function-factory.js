'use strict';

const Util = require('../commons-util');
const Errors = require('../commons-errors');
const path = require('path');

var cacheFunctionTransformation = {};

exports.createFunctionByName = (options) => {

    let name = options.functionName || null;
    let dir = options.dir || '';
    let fileFunctionPath = path.resolve(process.cwd() + `/functions/${dir}/${name}`);

    if (name) {

        try {        

            if (cacheFunctionTransformation[fileFunctionPath]) {
                Util.info(`Hit function=${fileFunctionPath} in Cache`);
                return cacheFunctionTransformation[fileFunctionPath];
            } else {
                let CreateFunction = require(fileFunctionPath);
                Util.info(`Created function=${name} path=${fileFunctionPath}`);
                cacheFunctionTransformation[fileFunctionPath] = CreateFunction;
                return CreateFunction;
            };
        } catch (error) {
            Util.error(`Error to create function name=${name} path=${fileFunctionPath}`, error);
            throw error;
        };

    } else {
        throw new Errors.ApiGatewayError("Error to create function");
    };

};