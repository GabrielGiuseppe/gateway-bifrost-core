'use strict';

var Util = require('../commons-util');
var Const = require('../commons-const');
var HttpOperation = require('../operations/http');
var FunctionOperation = require('../operations/function');
var MockOperation = require('../operations/mock');
var MonitorCheckOperation = require('../operations/monitor-check');

class OperationFlowFactory {

    constructor() {
    }

    /*
   iterate : IterateOperation,
   cached : CachedOperation,
   flow : ExecuteFlowOperation,
   render : RenderOperation,
   if : IfOperation,
   throw : ThrowOperation
   */

    static getRegisteredOperations(options) {
        switch (options.type) {
            case Const.FLOW_OPERATION_TYPE.HTTP:
                return {
                    New: HttpOperation,
                    description: `[${options.clientName}]/${options.path}`
                };
            case Const.FLOW_OPERATION_TYPE.FUNCTION:
                return {
                    New: FunctionOperation,
                    description: `${options.functionName}.js`
                };
            case Const.FLOW_OPERATION_TYPE.MOCK:
                return {
                    New: MockOperation,
                    description: options.file
                };
            case Const.FLOW_OPERATION_TYPE.MONITOR_CHECK:
                return {
                    New: MonitorCheckOperation,
                    description: options.clientName
                };
            default:
                Util.throwErrorIfItExists(new Error('Invalid operation type: ' + options.type));
                break;
        };
    };

    static createOperationByType(options, context) {

        let operation = this.getRegisteredOperations(options);

        Util.info(`Created operation=${options.type} for ${operation.description}`);
        return new operation.New(options, context);

    };

};
module.exports = OperationFlowFactory;
module.getRegisteredOperations = OperationFlowFactory.getRegisteredOperations;
module.createOperationByType = OperationFlowFactory.createOperationByType;