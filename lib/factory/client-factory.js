'use strict';

const Util = require('../commons-util');
const HttpClient = require('../clients/http-client');
const Environment = require('../environment');
const MappingClients = Environment.mappingClients;
const CacheClient = {};

var ClientFactory = (function() {

    function createByName(clientName) {

        if (!clientName) {
            Util.throwErrorIfItExists(new Error('The client name can not be empty.'));
        } else if (!MappingClients[clientName]) {
            Util.throwErrorIfItExists(new Error('Invalid client name=' + clientName));
        } else {
            var options = MappingClients[clientName];
            try {

                if (CacheClient[clientName]) {
                    return CacheClient[clientName];
                } else {
                    Util.info("Created client name=" + clientName);
                    var httpClient = new HttpClient(options);
                    CacheClient[clientName] = httpClient;
                    return httpClient;
                }
            } catch (error) {
                Util.throwErrorIfItExists(new Error('Invalid client name=' + clientName));
            }
        }
    }

    return {
        createByName: createByName
    };

})();

module.exports = ClientFactory;