'use strict';

var Util = require('../commons-util');
var Const = require('../commons-const');
var Function = require('../operations/function');
var BaseContext = require('../models/base-context');
const uuidv1 = require('uuid/v1');

class TransformationFunctionFactory {

    static create(name, context) {

        if (!name)
            return new FunctionDefault({
                type: Const.FLOW_OPERATION_TYPE.FUNCTION,
                functionName: Const.NOT_IMPLEMENTED_FUNCTION
            }, context);

        Util.info(`Created function name=${name}`);

        return new Function({
            type: Const.FLOW_OPERATION_TYPE.FUNCTION,
            functionName: name
        }, context);

    };

};

class FunctionDefault extends BaseContext {

    constructor(options, context) {
        super(uuidv1(), context);
        this.type = options.type;
        this.name = options.functionName;
    };

    doExecuteFunction(options, context, callback) {
        callback(null, options);
    };

    doExecuteFunctionWithoutCallback(options, context) {        
    };

};

module.exports = TransformationFunctionFactory;
module.create = TransformationFunctionFactory.create;