'use strict';

const Util = require('../commons-util');
var environment = require('../environment');
const Server = require('../services/server')

module.exports = {
    createServer: (options) => {
        try {
            return new Server(options || environment);
        } catch (error) {
            Util.throwErrorIfItExists(error);
        }
    }
};