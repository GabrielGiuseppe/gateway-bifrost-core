'use strict';

var winston = require('winston')
require('winston-insightops')
const os = require("os");
const _ = require('underscore');
const environment = require('./environment');
const hash = require('object-hash');
const crypto = require('crypto');

winston.setLevels(winston.config.syslog.levels);

var Util = function () { };

Util.config = {
    logger: environment.application.logger,
    applicationName: environment.application.appName || 'NO NAME',
    machineHostName: os.hostname(),
    env: process.env.NODE_ENV,
    stringify: {
        // If > 0 will truncate string values at this length
        truncationLength: 0,

        // Same list as com.vcmais.commons.utils.ToStringBuilder.EXCLUDE_FIELD_NAMES (commons-util project)
        // Case INsensitive. Can be a RegEx pattern (for example (new)?Password would match "password" and "newPassword")
        exclusions: ["accessKeyId", "secretAccessKey", "cpf", "pin", "password", "newPassword", "confirmPassword",
            "cvv", "cardKey", "encKey", "encPaymentData", "encryptionKey", "publicKey", "fingerprintKey",
            "accessToken", "file", "icon", "background", "photo", "Authorization", "cardNumber"],

        // If this field have a value, use it instead of whatever came along any of the fields above.
        // For example: { password: "xpto" } --> { "password": "**********" } (in case excludedKeyValue have its default value)
        excludedKeyValue: "***"
    }
};

Util.getLogger = function () {

    if (typeof this.logger !== 'undefined') {
        return this.logger;
    } else {
        return Util.configureLogger();
    }
};

var _printLogDefaults = {
    pid: process.pid,
    layer: Util.config.applicationName,
    machine: Util.config.machineHostName,
    env: Util.config.env
};

function _printLog(level, message, options) {
    Util.getLogger().log(level, message, _.defaults(_.clone(options), _printLogDefaults));
}

Util.configureLogger = function () {

    var transports = [];
    var debugEnabled = false;

    if (this.config.logger.transports) {
        for (var _transport in this.config.logger.transports) {

            var transportOptions = this.config.logger.transports[_transport];

            try {
                //winston.transports.Logentries
                var transport = eval("new winston.transports." + _transport + "(" + JSON.stringify(transportOptions) + ")");
                transports.push(transport);

                if (this.config.logger.debug) {
                    console.log("Transport: " + _transport);
                    console.log("  Options: " + JSON.stringify(transportOptions));
                    console.log("   Status:");
                    console.log(transport);
                    console.log("\n");
                }

                debugEnabled = debugEnabled || (transport.level === 'debug');

            } catch (e) {
                console.log("Cannot configure transport: " + _transport + ". Reason: " + e.message);
            }
        }
    }

    if (transports.length === 0) {
        console.log("[WARN] Logger: None transports were found. Automatically configuring Console transport.");
        transports.push(new winston.transports.Console());
    }

    this.logger = new (winston.Logger)({
        transports: transports,
        exitOnError: false
    });

    this.logger.debugEnabled = debugEnabled;

    return this.logger;

};

Util.log = function () {

    var self = this;

    try {
        var message = Array.prototype.slice.call(arguments).join(' ');
        Util.info(message);
    } catch (error) {
        try {
            Util.getLogger().log(
                (error === null) ? 'Error' : error.message || error, {
                env: self.config.env,
                machine: self.config.machineHostName,
                layer: self.config.applicationName,
                stackTrace: ((error !== null) ? Util.getErrorStack(error) : null)
            });
        } catch (error) { }
    }
};

Util.getErrorStack = function (error) {

    var errorStack = error.stack || error.message || error;

    if (error.cause) {
        errorStack += "\n" + Util.getErrorStack(error.cause);
    }

    return errorStack;
};

Util.debug = function (message, options) {
    _printLog('debug', message, options);
};

Util.warning = function (message, options) {
    _printLog('warn', message, options);
};

Util.info = function (message, options) {
    _printLog('info', message, options);
};

Util.error = function (message, options) {
    _printLog('error', message, options);
};

Util.throwErrorIfItExists = function (error) {
    if (error) {
        //Executando o throw o Step pula pro próximo FLOW
        //Util.error(error);
        throw error;
    }
};

Util.encodeHexStringToBase64 = function (string) {
    return new Buffer(string, 'hex').toString('base64');
};


Util.encodeBufferToBase64 = function (buffer) {
    return buffer.toString('base64');
};

Util.decodeBase64ToBuffer = function (string) {
    return new Buffer(string, 'base64');
};

Util.decodeBase64ToHex = function (string) {
    return new Buffer(string, 'base64').toString('hex');
};

Util.encodeBase64 = function (string) {
    return new Buffer(string).toString('base64');
};

Util.decodeBase64 = function (string) {
    return new Buffer(string, 'base64').toString('ascii');
};

Util.format = function (str) {
    var args = [].slice.call(arguments, 1);

    return str.replace(/%s/g, function () {
        return args.shift();
    });
};

Util.extend = function (target) {
    var sources = [].slice.call(arguments, 1);
    sources.forEach(function (source) {
        for (var prop in source) {
            target[prop] = source[prop];
        }
    });
    return target;
};

Util.isJSONValid = function (str) {
    try {
        if (str instanceof Object) {
            return true;
        } else {
            JSON.parse(str);
        }
    } catch (e) {
        return false;
    }
    return true;
};

Util.stringify = function (objJSON) {

    if (Util.isJSONValid(objJSON)) {
        return JSON.stringify(objJSON);
    } else {
        return objJSON;
    };

};

Util.tryParseJSON = function (jsonData) {
    try {
        if (jsonData instanceof Object) {
            return jsonData;
        } else {
            return JSON.parse(jsonData);
        }
    } catch (e) {
        return jsonData;
    }
};

Util.parseJSON = function (jsonData) {
    try {
        if (jsonData instanceof Object) {
            return jsonData;
        } else {
            return JSON.parse(jsonData);
        }
    } catch (e) {
        return {
            "success": false,
            "message": e.message,
            "jsonData": jsonData
        };
    }
};

Util.toQueryString = function (obj) {
    return _.map(obj, function (v, k) {
        return encodeURIComponent(k) + '=' + encodeURIComponent(v);
    }).join('&');
};

Util.setValue = function (sourceObject, variableName, value) {

    var levels = variableName.split('.'),
        newSourceObject = sourceObject || {},
        source = newSourceObject;

    for (var i = 0; i < levels.length - 1; i++) {
        source = sourceObject[levels[i]];

        if (!source) {
            sourceObject[levels[i]] = {};
            source = sourceObject[levels[i]];
        }
    }

    if (source[levels[levels.length - 1]]) {
        source[levels[levels.length - 1]] = _.extend(source[levels[levels.length - 1]], value);
    } else {
        source[levels[levels.length - 1]] = value;
    }

    return newSourceObject;

};

Util.stringifyInfo = function (objJSON) {

    var cache = [];
    var self = this;
    var regex = new RegExp('^(' + self.config.stringify.exclusions.join('|') + ')$', 'i');

    objJSON = Util.tryParseJSON(objJSON);

    return JSON.stringify(objJSON, function (key, value) {
        if (regex.test(key)) {
            if (self.config.stringify.excludedKeyValue) {
                value = self.config.stringify.excludedKeyValue;
            } else {
                return;
            }
        }

        if (value !== null && value !== undefined) {
            if (typeof value === 'object') {
                if (cache.indexOf(value) !== -1) {
                    // Circular reference found, discard key
                    return;
                }
                // Store value in our collection
                cache.push(value);
            } else if (typeof value === 'string' && self.config.stringify.truncationLength > 0) {
                return value.length > self.config.stringify.truncationLength ? value.substring(0, self.config.stringify.truncationLength) + '... [truncated]' : value;
            }
        }

        return value;
    });
};

Util.nonNull = function (object) {
    object = object || {};
    if (typeof object === 'object') {
        return !(Object.keys(object).length === 0 && object.constructor === Object);
    } else {
        return (object);
    }
};

Util.isNull = function (object) {
    return _.isNull(object);
};

Util.isEmpty = function (object) {
    return _.isEmpty(object);
};

Util.getEncryptionKey = function (context) {

    if (context.service.security) {
        return context.security.encryptionKey;
    } else {
        //GET ALL RESULT IN SERVICE.RESULT
        return context.service.result.encryptionKey;
    }

};

Util.getPublicKey = function (context) {

    if (context.service.security) {
        return context.security.publicKey;
    } else {
        return Util.decodeBase64(context.headers['x-device-public-key']);
    }

};

Util.getDeviceDNA = function (context) {

    if (context.service.security) {
        return context.security.deviceDNA;
    } else {
        return context.service.result.device.dna;
    }

};

Util.generateKey = function (objectKey) {
    return hash(objectKey);
};

Util.shallowCopy = function (obj) {
    if (!obj) {
        return obj;
    }
    var copy = {};
    Object.keys(obj).forEach(function forEach(k) {
        copy[k] = obj[k];
    });
    return copy;
};

Util.formatJSON = function (req, res, body) {
    var data = body ? JSON.stringify(body) : 'null';
    // Setting the content-length header is not a formatting feature and should
    // be separated into another module
    res.setHeader('Content-Length', Buffer.byteLength(data));
    return data;
};

Util.responseTimeInMillis = function (initialTime, endTime) {
    return ((endTime || new Date()) - initialTime);
};

function parseDuration(duration) {

    let remain = duration

    let days = Math.floor(remain / (1000 * 60 * 60 * 24))
    remain = remain % (1000 * 60 * 60 * 24)

    let hours = Math.floor(remain / (1000 * 60 * 60))
    remain = remain % (1000 * 60 * 60)

    let minutes = Math.floor(remain / (1000 * 60))
    remain = remain % (1000 * 60)

    let seconds = Math.floor(remain / (1000))
    remain = remain % (1000)

    let milliseconds = remain

    return {
        days,
        hours,
        minutes,
        seconds,
        milliseconds
    };
}

function formatTime(o, useMilli = false) {

    let parts = [];

    if (o.days) {
        let ret = o.days + ' day'
        if (o.days !== 1) {
            ret += 's'
        }
        parts.push(ret)
    };

    if (o.hours) {
        let ret = o.hours + ' hour'
        if (o.hours !== 1) {
            ret += 's'
        }
        parts.push(ret)
    }
    if (o.minutes) {
        let ret = o.minutes + ' minute'
        if (o.minutes !== 1) {
            ret += 's'
        }
        parts.push(ret)

    };

    if (o.seconds) {
        let ret = o.seconds + ' second'
        if (o.seconds !== 1) {
            ret += 's'
        }
        parts.push(ret)
    };

    if (useMilli && o.milliseconds) {
        let ret = o.milliseconds + ' millisecond'
        if (o.milliseconds !== 1) {
            ret += 's'
        }
        parts.push(ret)
    };

    if (parts.length === 0) {
        return 'instantly'
    } else {
        return parts.join(' ')
    };

};

Util.formatDuration = function (duration) {
    let time = parseDuration(duration)
    return formatTime(time, true)
};

Util.defaultDateISOString = function () {
    return new Date('1900-01-01T00:00:00.000Z').toISOString();
};

Util.parseCookies = (headers) => {
    var list = {},
        rc = headers.cookie;

    rc && rc.split(';').forEach(function (cookie) {
        var parts = cookie.split('=');
        list[parts.shift().trim()] = decodeURI(parts.join('='));
    });

    return list;

};

Util.parseSetCookies = (headers) => {
    var list = {},
        rc = headers['set-cookie'];

    rc && rc.map((item) => {
        item.split(';').map((cookie) => {
            var parts = cookie.split('=');
            list[parts.shift().trim()] = decodeURI(parts.join('='));
        });
    });

    return list;

};

Util.zeroPad = (number, places) => {
    var zero = places - number.toString().length + 1;
    return Array(+(zero > 0 && zero)).join("0") + number;
};

Util.underscore = () => {
    return _;
};

Util.checksum = (str, algorithm, encoding) => {
    return crypto
        .createHash(algorithm || 'md5')
        .update(str, 'utf8')
        .digest(encoding || 'hex')
};

Util.formatNumberFlatToDecimal = (number, decimalSize = 2) => {

    try {

        let n = number.toString();
        let s = n.length;
        let d = decimalSize;

        return parseFloat(`${n.substring(0, s - d)}.${n.substring(s - d, s)}`) || 0.00;

    } catch (error) {
        Util.error("Error to format number flat to float", error);
        return 0.00;
    };

};

Util.compileTemplate = (data, template) => {
    let compiled = _.template(template);
    return compiled(data);
}

Util.isUrl = (url) => {

    if (!_.isString(url))
        return false;

    if (url.toLowerCase().indexOf('localhost') != -1)
        return true;

    if (url.toLowerCase().startsWith('https://') ||
        url.toLowerCase().startsWith('http://')) {
        return true;
    } else {
        return false;
    }
}

Util.getObjectCache = async (context, keyName, key) => {
    return new Promise((resolved, rejected) => {
        context.orchestrator.redisClient.get(keyName, key, (error, entry) => {
            if (error) rejected(error);
            resolved(entry || {});
        });
    });
};

Util.putObjectCache = async (context, keyName, key, data, ttl) => {
    context.orchestrator.redisClient.put(keyName, key, data, ttl || (60 * 1)); //1min Default
};


Util.getObjectCacheByCacheName = async (context, cacheName) => {
    return new Promise((resolved, rejected) => {
        context.orchestrator.redisClient.getKeys(cacheName, (error, entry) => {
            if (error) rejected(error);
            resolved(entry || {});
        });
    });
};

module.exports = Util;