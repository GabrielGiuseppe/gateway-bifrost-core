'use strict';

class Router {

    constructor(app) {
        this.app = app;
    };

    addRoute(route, handler) {

        this.app[route.getMethod()](route.getPath(),
            (request, response, next) => {
                handler.handleRequest(request, response, next);
            }
        );

    };
}

module.exports = Router;