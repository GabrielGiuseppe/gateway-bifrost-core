'use strict';

var Step = require('step');
var jwtUtil = require('./jwt-util');

var securityValidation = (function() {

    function execute(context, callback) {

        if (context.service.security) {

            var xAccessToken = context.headers['x-access-token'];

            Step(
                function() {
                    jwtUtil.validateAccessToken(xAccessToken, this);
                },
                function(error, decodedPayload) {
                    if (!error) {
                        updateContext(context, decodedPayload);
                    }
                    return callback(error);
                }
            );

        } else {
            return callback();
        }

    }

    function updateContext(context, decodedPayload) {
        context.security.setCustomerUUID(decodedPayload.uuid);
        context.security.setDeviceDNA(decodedPayload.deviceDNA);
        context.security.setDevicePublicKeyDisgest(decodedPayload.devicePublicKeyDisgest);
        context.security.setAccessToken(decodedPayload.accessToken);
    }

    return {
        execute: execute
    };

})();


module.exports = securityValidation;