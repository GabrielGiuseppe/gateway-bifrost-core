'use strict';

const Util = require('./commons-util');
const Const = require('./commons-const');

var VALIDATION_ERROR_CODE = 400,
    UNAUTHORIZED_ERROR_CODE = 401,
    FORBIDDEN_ERROR_CODE = 403,
    NOT_FOUND_ERROR_CODE = 404,
    ENOTFOUND_ERROR = 'ENOTFOUND',
    ETIMEDOUT_ERROR = 'ETIMEDOUT',
    ENOTFOUND_ERROR_PTBR = 'Não encontrado',
    ETIMEDOUT_ERROR_PTBR = 'Tempo esgotado',
    REQUEST_TIMEOUT_ERROR_CODE = 408,
    NOT_ACCEPTABLE_ERROR_CODE = 406,
    UNAVAILABLE_ERROR_CODE = 503,
    INTERNAL_SERVER_ERROR_CODE = 500,
    DEFAULT_ERROR = 'default',
    DEFAULT_SECURITY_ERROR_MESSAGE = 'Request Unauthorized.',
    DEFAULT_VALIDATION_ERROR_MESSAGE = 'Invalid Arguments.',
    DEFAULT_NOT_FOUND_MESSAGE = 'Not Found.',
    DEFAULT_REQUEST_TIMEOUT_MESSAGE = 'Request Timeout.',
    DEFAULT_ERROR_MESSAGE = 'Error processing the Request.',
    DEFAULT_ERROR_PTBR = 'Padrão',
    DEFAULT_SECURITY_ERROR_MESSAGE_PTBR = 'Solicitação não autorizada',
    DEFAULT_VALIDATION_ERROR_MESSAGE_PTBR = 'Argumentos inválidos',
    DEFAULT_NOT_FOUND_MESSAGE_PTBR = 'Não encontrado',
    DEFAULT_REQUEST_TIMEOUT_MESSAGE_PTBR = 'Tempo esgotado',
    DEFAULT_ERROR_MESSAGE_PTBR = 'Erro ao processar a solicitação',
    DEFAULT_HANDLERS = {
        [VALIDATION_ERROR_CODE]: (error) => {
            return Errors.handleValidationError(error, VALIDATION_ERROR_CODE);
        },
        [UNAUTHORIZED_ERROR_CODE]: (error) => {
            return Errors.handleSecurityError(error, UNAUTHORIZED_ERROR_CODE);
        },
        [FORBIDDEN_ERROR_CODE]: (error) => {
            return Errors.handleSecurityError(error, FORBIDDEN_ERROR_CODE);
        },
        [NOT_FOUND_ERROR_CODE]: (error) => {
            return Errors.handleNotFoundError(error);
        },
        [ENOTFOUND_ERROR]: (error) => {
            return Errors.handleNotFoundError(error);
        },
        [NOT_ACCEPTABLE_ERROR_CODE]: (error) => {
            return Errors.handleValidationError(error, NOT_ACCEPTABLE_ERROR_CODE);
        },
        [UNAVAILABLE_ERROR_CODE]: (error) => {
            return Errors.handleDefaultError(error);
        },
        [REQUEST_TIMEOUT_ERROR_CODE]: (error) => {
            return Errors.handleRequestTimeoutError(error, REQUEST_TIMEOUT_ERROR_CODE);
        },
        [ETIMEDOUT_ERROR]: (error) => {
            return Errors.handleRequestTimeoutError(error, REQUEST_TIMEOUT_ERROR_CODE);
        },
        [DEFAULT_ERROR]: (error) => {
            return Errors.handleDefaultError(error);
        }
    };


function validateCause(cause) {

    if (cause) {

        if (cause instanceof TypeError ||
            cause instanceof ReferenceError) {
            return {
                name: cause.name,
                message: cause.message,
                stack: cause.stack
            };
        } else {
            return cause.hasOwnProperty("message") ? cause.message : cause;
        };

    };

    return null;

};

class BaseError extends Error {
    constructor(message, cause) {
        super();
    }
};

class ApiGatewayError extends BaseError {
    constructor(message, cause) {
        super();
        //this.name = 'ApiGatewayError';
        this.nome = 'ErroApiGateway';
        //this.propagatable = true;
        this.propagavel = true;
        //this.responseCode = INTERNAL_SERVER_ERROR_CODE;
        this.codigoResposta = INTERNAL_SERVER_ERROR_CODE;
        //this.message = message || DEFAULT_ERROR_MESSAGE_PTBR;
        this.mensagem = message || DEFAULT_ERROR_MESSAGE_PTBR;
        //this.cause = validateCause(cause);
        this.causa = validateCause(cause);
        Error.captureStackTrace(this, ApiGatewayError);
    };
};

class ValidationError extends BaseError {
    constructor(message, cause, responseCode) {
        super();
        //this.name = 'ValidationError';
        this.nome = 'ErroValidação';
        //this.propagatable = true;
        this.propagavel = true;
        //this.responseCode = responseCode || VALIDATION_ERROR_CODE;
        this.codigoResposta = responseCode || VALIDATION_ERROR_CODE;
        //this.message = message || DEFAULT_VALIDATION_ERROR_MESSAGE_PTBR;
        this.mensagem = message || DEFAULT_VALIDATION_ERROR_MESSAGE_PTBR;
        //this.cause = validateCause(cause);
        this.causa = validateCause(cause);
        Error.captureStackTrace(this, ValidationError);
    };
};

class SecurityError extends BaseError {
    constructor(message, cause, responseCode) {
        super();
        //this.name = 'SecurityError';
        this.nome = 'ErroSegurança';
        //this.propagatable = true;
        this.propagavel = true;
        //this.responseCode = responseCode || UNAUTHORIZED_ERROR_CODE;
        this.codigoResposta = responseCode || UNAUTHORIZED_ERROR_CODE;
        //this.message = message || DEFAULT_SECURITY_ERROR_MESSAGE_PTBR;
        this.mensagem = message || DEFAULT_SECURITY_ERROR_MESSAGE_PTBR;
        //this.cause = validateCause(cause);
        this.causa = validateCause(cause);
        Error.captureStackTrace(this, SecurityError);
    };
};

class NotFoundError extends BaseError {
    constructor(message, cause) {
        super();
        //this.name = 'NotFoundError';
        this.nome = 'ErroNãoEncontrado';
        //this.propagatable = true;
        this.propagavel = true;
        //this.responseCode = NOT_FOUND_ERROR_CODE;
        this.codigoResposta = NOT_FOUND_ERROR_CODE;
        //this.message = message || DEFAULT_NOT_FOUND_MESSAGE_PTBR;
        this.mensagem = message || DEFAULT_NOT_FOUND_MESSAGE_PTBR;
        //this.cause = validateCause(cause);
        this.causa = validateCause(cause);
        Error.captureStackTrace(this, NotFoundError);
    };
};

class RequestTimeoutError extends BaseError {
    constructor(message, cause) {
        super();
        //this.name = 'RequestTimeout';
        this.nome = 'TempoEsgotado';
        //this.propagatable = true;
        this.propagavel = true;
        //this.responseCode = REQUEST_TIMEOUT_ERROR_CODE;
        this.codigoResposta = REQUEST_TIMEOUT_ERROR_CODE;
        //this.message = message || DEFAULT_REQUEST_TIMEOUT_MESSAGE_PTBR;
        this.mensagem = message || DEFAULT_REQUEST_TIMEOUT_MESSAGE_PTBR;
        //this.cause = validateCause(cause);
        this.causa = validateCause(cause);
        Error.captureStackTrace(this, RequestTimeoutError);
    };
};


class Errors {

    static handlers() {
        return DEFAULT_HANDLERS;
    }

    static handleError(error) {

        if (error instanceof BaseError) return error;

        let errorCode = DEFAULT_ERROR;

        if (error.hasOwnProperty('code'))
            errorCode = error.code;

        if (error.hasOwnProperty('responseCode'))
            errorCode = error.responseCode;

        let handler = this.handlers()[errorCode];

        if (handler) {
            return handler(error);
        } else {
            return this.handleDefaultError(error);
        };

    };

    static handleSecurityError(error, responseCode) {
        return new SecurityError(DEFAULT_SECURITY_ERROR_MESSAGE_PTBR, error, responseCode);

    };

    static handleValidationError(error, responseCode) {
        return new ValidationError(DEFAULT_VALIDATION_ERROR_MESSAGE_PTBR, error, responseCode);
    };

    static handleNotFoundError(error) {
        return new NotFoundError(DEFAULT_NOT_FOUND_MESSAGE_PTBR, error);

    };

    static handleRequestTimeoutError(error) {
        return new RequestTimeoutError(DEFAULT_REQUEST_TIMEOUT_MESSAGE_PTBR, error);

    };

    static handleDefaultError(error) {
        return new ApiGatewayError(DEFAULT_ERROR_MESSAGE_PTBR, error);

    };

};

function createFunctionError(error) {

    if (error instanceof BaseError) return error;

    let defaultError = {
        responseCode: INTERNAL_SERVER_ERROR_CODE,
        message: error.hasOwnProperty('message') ? error.message : DEFAULT_ERROR_MESSAGE_PTBR,
        stack: error.hasOwnProperty('stack') ? error.stack : error
    };

    return defaultError;

};

function createHttpError(error, response) {

    let defaultError = {
        code: INTERNAL_SERVER_ERROR_CODE,
        message: DEFAULT_ERROR_MESSAGE_PTBR
    };

    let testCase = Util.nonNull(error) + '_' + Util.nonNull(response);

    switch (testCase) {
        case 'false_true':
            defaultError.code = response.statusCode || Const.DEFAULT_ERROR_CODE;
            defaultError.message = Util.tryParseJSON(error || response.body || {});
            break;
        case 'true_false':
            defaultError.code = error.code || Const.DEFAULT_ERROR_CODE;
            defaultError.message = Util.tryParseJSON(error);
            break
        default:
            break;
    }

    return defaultError;

};

function createSimpleError(message) {
    return {
        sucesso: false,
        descricao: message,
    };
};

function createCustomError(code, message) {
    return {
        errors: [{
            error: code,
            message: message
        }]
    };
};

function createCustomErrors(errors) {
    return {
        errors: errors
    };
};

module.exports = {
    ApiGatewayError: ApiGatewayError,
    ValidationError: ValidationError,
    SecurityError: SecurityError,
    NotFoundError: NotFoundError,
    RequestTimeoutError: RequestTimeoutError,
    Errors: Errors,
    CreateSimpleError: createSimpleError,
    CreateCustomError: createCustomError,
    CreateCustomErrors: createCustomErrors,
    CreateHttpError: createHttpError,
    CreateFunctionError: createFunctionError,
    BaseError: BaseError
}