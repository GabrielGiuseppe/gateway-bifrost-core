'use strict';

const Util = require('../commons-util');
const path = require('path');
const restify = require('restify');
const ServiceGroup = require('../models/service-group');
const Router = require('../router');
const ServiceRoute = require('../models/service-route');
const HandleRoute = require('../handle-route');
const OrchestrationFlow = require('../orchestration-flow');
const commonsCache = require('../commons-cache');
const commonsElasticSearch = require('../commons-elasticsearch');
const ServiceContext = require('../models/service-context');
const templateMonitorCheckRoute = require('./template-monitorcheck-route');
const _ = require('underscore');

class Server {

    constructor(environment) {

        this.environment = environment;
        this.initialTime = new Date();
        this.port = this.environment.application.serverPort || 8080;

        //Server APP Retify
        this.app = restify.createServer({
            name: environment.application.appName,
            ignoreTrailingSlash: true
        });

        //Grupo de Servicos Expostos na API Gateway
        this.generateMonitorCheckRoutes(environment);

        this.listServiceGroup = createListServiceGroup(environment.mappingServices);

        this.redisClient = commonsCache.createClient(environment.application);
        this.elasticSearch = commonsElasticSearch.createClient(environment.application);
        this.restify = restify;
        this.validateFlowAndFunctions();

    };

    generateMonitorCheckRoutes(environment) {

        let serviceHealthCheck = {
            basePath: '/healthcheck/',
            description: 'Health Clients',
            version: 'V1',
            services: []
        };

        let mappingClients = environment.mappingClients;

        Object.keys(mappingClients).map(function (clientName) {

            let currentClientConfig = mappingClients[clientName];
            serviceHealthCheck.services.push(new templateMonitorCheckRoute(clientName, currentClientConfig));

        });

        environment.mappingServices.push(serviceHealthCheck);

    };

    validateFlowAndFunctions() {

        Util.warning('Validate Flows and Services');

        try {

            this.listServiceGroup.map(group => {
                return group.services.map(service => {
                    return new ServiceContext(service, group, null);;
                });
            });

        } catch (error) {
            Util.throwErrorIfItExists(error);
        }

    };

    start() {

        let self = this;

        //SECURITY HEADERS, UUID, QUERYPARSER, BODYPARSER, MORGAN
        require('../default-routes')(this, this.restify);

        //CRIA AS ROTAS  
        this.registerServicesRoutesOnServer();

        this.app.listen(this.port, function (error) {
            if (error) {
                Util.throwErrorIfItExists(error);
            } else {
                var listenMsg = '\n\n ' + self.environment.application.appName.toUpperCase();
                listenMsg += '\n +++++++++++++++++++++++++++++++++++++++++++++++++++++';
                listenMsg += '\n\n Environment ' + self.environment.environment.toUpperCase();
                listenMsg += '\n\n Listening on port ' + self.port;
                listenMsg += '\n\n http://localhost:' + self.port + '/';
                listenMsg += '\n\n Groups Services: ' + self.listServiceGroup.length;
                listenMsg += '\n\n Services: ' + self.listServiceGroup.map(group => {
                    return group.services.length;
                }).reduce((a, b) => a + b, 0);
                listenMsg += '\n\n Flows: ' + self.listServiceGroup.map(group => {
                    return group.services.map(service => {
                        return service.flow.length;
                    }).reduce((a, b) => a + b, 0);
                }).reduce((a, b) => a + b, 0);
                listenMsg += '\n\n Inicialization in ' + (new Date() - self.initialTime) + ' ms \n\n';
                listenMsg += '+++++++++++++++++++++++++++++++++++++++++++++++++++++ \n\n';
                Util.warning(listenMsg);
            }
        });

    };

    registerServicesRoutesOnServer() {

        let self = this;

        let _router = new Router(self.app);

        self.listServiceGroup.forEach(function (_serviceGroup) {

            Util.warning('Registering service group',
                {
                    description: _serviceGroup.description,
                    basePath: _serviceGroup.basePath,
                    totalServices: _serviceGroup.services.length,
                });

            _serviceGroup.services.forEach(function (_serviceOptions) {

                let route = new ServiceRoute({
                    path: path.join('/', _serviceGroup.basePath, '/', _serviceOptions.route || ''),
                    method: _serviceOptions.method,
                    security: _serviceOptions.security
                });

                Util.warning('Registering route',
                    {
                        serviceName: _serviceOptions.name,
                        method: _serviceOptions.method.toUpperCase(),
                        path: route.getPath(),
                        totalFlows: _serviceOptions.flow.length
                    });

                let _orchestrationFlow = new OrchestrationFlow(self.redisClient);

                let _handleRoute = new HandleRoute(
                    _orchestrationFlow,
                    _serviceOptions,
                    _serviceGroup,
                    self.elasticSearch);

                _router.addRoute(route, _handleRoute);

            });
        });
    };
};

function createListServiceGroup(mappingServices) {

    return mappingServices.map(serviceGroup => {
        return new ServiceGroup(serviceGroup);
    });
};

// Hook into commonJS module systems
module.exports = Server;