'use strict';

const path = require('path')

class templateMonitorCheckRoute {

    constructor(clientName, options) {

        this.name = `${clientName}HealthCheck`;
        this.description = options.description;
        this.method = 'GET';
        this.route = `${clientName}`;
        this.flow = []

        if (options.hasOwnProperty('healthCheck')) {
            this.flow.push(this.addFlowHealth(clientName, options));
        };

        if (options.hasOwnProperty('infoCheck')) {
            this.flow.push(this.addFlowInfo(clientName, options));
        };

    };

    addFlowInfo(clientName, options) {
        return {
            resultVariable: 'info',
            operation: {
                clientName: clientName,
                path: options.infoCheck,
                type: 'monitor'
            }
        };
    };

    addFlowHealth(clientName, options) {
        return {
            resultVariable: 'health',
            operation: {
                clientName: clientName,
                path: options.healthCheck,
                type: 'monitor'
            }
        };
    };

};

module.exports = templateMonitorCheckRoute;